import { createMuiTheme } from "@material-ui/core/styles";
// import { orange, green, indigo, pink, red, grey } from "@material-ui/core/colors";
import { orange, green, indigo, grey } from "@material-ui/core/colors";
import { PaletteOptions } from "@material-ui/core/styles/createPalette";

const palette: PaletteOptions = {
  primary: {
    main: indigo[500],
  },
  secondary: {
    main: grey[600],
  },
  warning: {
    main: orange[500],
  },
  success: {
    main: green[500],
  },
};
const baseTextShadow = '1px 1px 2px rgba(0, 0, 0, 0.2)';
const baseDropShadow = 'drop-shadow( 1px 1px 2px rgba(0, 0, 0, 0.5))';
const themeDef = createMuiTheme({
  palette: palette,
});

export const theme = createMuiTheme({
  palette: palette,
  typography: {
    fontFamily: 'Kanit',
    fontSize: 13,
  },
  overrides: {

    MuiTypography: {
      root: {
        textShadow: baseTextShadow,
        '& .divider-header': {
          background: themeDef.palette.text.secondary,
          boxShadow: baseTextShadow,
        }
      },
      body1: {
        fontSize: 13,
        textShadow: 'none',
      },
      body2: {
        textShadow: 'none',
      },
      caption: {
        textShadow: 'none',
      },
    },
    MuiTab: {
      root: {
        textShadow: baseTextShadow,
      },
    },
    MuiIconButton: {
      root: {
        padding: themeDef.spacing(1),
      },
    },
    MuiButton: {
      contained: {
        WebkitFilter: baseDropShadow,
        filter: baseDropShadow,
        '&:disabled': {
          boxShadow: baseTextShadow,
          WebkitFilter: 'none !important' as any,
          filter: 'none !important' as any,
          textShadow: 'none !important' as any,
        },
      },
      text: {
        textShadow: baseTextShadow,
      },
      label: {
        textShadow: baseTextShadow,
      },
      root: {
      },
    },
    MuiSvgIcon: {
      root: {
        WebkitFilter: baseDropShadow,
        filter: baseDropShadow,
      },
    },
    MuiTableHead: {
      root: {
        [`& th`]: {
          position: 'sticky',
          whiteSpace: 'nowrap',
          background: themeDef.palette.primary.main,
          color: 'white',
          fontSize: 14,
          padding: themeDef.spacing(1.5, 1),
        },
        [`& tr:nth-child(1) th:nth-child(1)`]: {
          borderTopLeftRadius: themeDef.spacing(0.5),
        },
        [`& tr:nth-child(1) th:nth-last-child(1)`]: {
          borderTopRightRadius: themeDef.spacing(0.5),
        },
        textShadow: baseTextShadow,
      }
    },
    MuiTableCell: {
      root: {
        fontSize: 13,
        padding: themeDef.spacing(1.5, 1),
      }
    },
    MuiTableSortLabel: {
      root: {
        color: 'white',
        '&:hover': {
          color: 'white'
        },
        '&:focus': {
          color: 'white'
        }
      },
      active: {
        fontWeight: 'bold',
        color: 'white !important'
      }
    },
    MuiTablePagination: {
      root: {
        boxShadow: '0 -8px 10px -5px rgba(0, 0, 0, 0.20)',
      },
      toolbar: {
        minHeight: 45,
      },
    },
    MuiListItemText: {
      root: {
        textShadow: baseTextShadow,
      },
      primary: {
        textShadow: baseTextShadow,
      }
    },
    MuiListItemIcon: {
      root: {
        minWidth: 0,
        padding: themeDef.spacing(0, 1),
      },
    },
    MuiFormLabel: {
      asterisk: {
        color: '#db3131',
        // float: 'left',
        // margin: '1px 5px 0px -5px',
        '&$error': {
          color: '#db3131'
        },
      }
    }
  }
});
