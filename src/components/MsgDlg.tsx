import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import {
  Button,
  makeStyles,
  Theme,
  withStyles,
  ThemeProvider,
  Typography,
} from "@material-ui/core";

import MuiDialog, { DialogProps } from "@material-ui/core/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";

import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import WarningOutlineIcon from "@material-ui/icons/WarningOutlined";
import InfoOutlineIcon from "@material-ui/icons/InfoOutlined";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";

import { v4 as uuidv4 } from "uuid";
import { theme } from "../Theme/Theme";
import { ResponseViewModel } from "../model";
import { isNullOrUndefined } from "util";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { CSSProp } from "styled-components";

const useStyles = makeStyles((theme: Theme) => ({
  successIcon: {
    fontSize: 100,
    color: theme.palette.success.light,
  },
  errorIcon: {
    fontSize: 100,
    color: theme.palette.error.light,
  },
  warningIcon: {
    fontSize: 100,
    color: theme.palette.warning.light,
  },
  infoIcon: {
    fontSize: 100,
    color: theme.palette.info.light,
  },
  dialogAction: {
    justifyContent: "center",
  },
  hidden: {
    display: 'none',
  },

  //================== Button =====================
  btn_cancel: {
    background: theme.palette.grey[400],
    color: '#000',
    "&:hover": {
      background: theme.palette.grey[500],
    },
  },
  btn_delete: {
    background: theme.palette.error.main,
    color: "#FAFAFA",
    "&:hover": {
      background: theme.palette.error.dark,
    },
  },
  btn_ok: {
    background: theme.palette.primary.main,
    color: "#FAFAFA",
    "&:hover": {
      background: theme.palette.primary.dark,
    },
  },
  btn_close: {
    background: theme.palette.primary.main,
    color: "#FAFAFA",
    "&:hover": {
      background: theme.palette.primary.dark,
    },
  },
}));

//===============================================================================

interface CustomButton {
  text?: string,
  onClick?: () => void,
  style?: CSSProperties,
  autoFocus?: boolean,
  autoClose?: boolean,
  buttonStyle?: keyof { 'btn_ok', 'btn_delete', 'btn_close', 'btn_cancel' },
}

interface MsgDlgProps {
  // response?: ResponseViewModel,
  response?: boolean,
  onClose?: () => void,
  status?: keyof { 'success', 'error', 'warning', 'info' },
  title?: string,
  message?: string | JSX.Element,
  maxWidth?: DialogProps['maxWidth'],
  customButton?: CustomButton[],
  actionsAlign?: keyof { 'flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'initial', 'inherit' },
};

interface ConfirmDlgProps {
  onConfirm: () => void,
  onClose?: () => void,
  confirmButtonText?: string,
  status?: keyof { 'error', 'warning' },
  title?: string,
  message?: string | JSX.Element,
  maxWidth?: DialogProps['maxWidth'],
  customButton?: CustomButton[],
  actionsAlign?: keyof { 'flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'initial', 'inherit' },
};

//===============================================================================

export const Dialog = withStyles((theme: Theme) => ({
  root: {
    '& .MuiDialog-container': {
      zIndex: 9999,
    },
    '& .MuiDialog-paper': {
      padding: theme.spacing(1),
      justifyContent: 'center',
      minHeight: '30%',
      minWidth: 'min-content',
    },
  },
}))(MuiDialog);

export const DialogTitle = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(1),
    textAlign: "center",
  },
}))(MuiDialogTitle);

export const DialogContent = withStyles((theme: Theme) => ({
  root: {
    // padding: theme.spacing(2),
    textAlign: "center",
  },
}))(MuiDialogContent);

export const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    '& .MuiButton-root:not(:first-child)': {
      marginLeft: theme.spacing(1.5),
    },
  },
}))(MuiDialogActions);

//===============================================================================

export const MsgDlg = (props: MsgDlgProps) => {
  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <CustomDialog {...props} key={uuidv4()} />
    </ThemeProvider>,
    document.getElementById('MsgDlg'));
};

//===============================================================================

export const ResultDlg = (response: ResponseViewModel, props?: MsgDlgProps) => {

  let params = {};

  if (response) {
    params = {
      status: (response.result ? 'success' : 'error'),
      title: (!isNullOrUndefined(response.error) && (response.error !== "") ? response.error : (response.result ? 'success' : 'error')),
      message: response.message || response.data?.message,
      response: true,
      ...props,
    };
  } else {
    params = {
      ...props
    };
  }

  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <CustomDialog {...params} key={uuidv4()} />
    </ThemeProvider>,
    document.getElementById('MsgDlg'));
};

//===============================================================================

export const ConfirmDlg = (props: ConfirmDlgProps) => {
  const { title, status, ...other } = props;

  let params: ConfirmDlgProps = {
    status: status || "warning",
    title: title || "Are you sure ?",
    // message: message || "You want to delete this item ?",
    ...other,
  };

  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <CustomDialog {...params} key={uuidv4()} />
    </ThemeProvider>,
    document.getElementById('MsgDlg'));
};

//===============================================================================

const CustomDialog = (props: any) => {
  const { status, title, message, maxWidth, confirmButtonText, onConfirm, onClose, customButton, actionsAlign, response } = props;
  const [openDlg, setOpenDlg] = useState(true);
  const classes = useStyles();

  const handleClose = (autoClose: boolean = true) => {
    if (onClose) onClose();
    if (!isNullOrUndefined(autoClose) && autoClose) setOpenDlg(false);
  };

  const textStatus = {
    success: { title: 'Success', icon: <CheckCircleOutlineIcon className={classes.successIcon} /> },
    error: { title: 'Error !!!', icon: <ErrorOutlineIcon className={classes.errorIcon} /> },
    warning: { title: 'Warning !!!', icon: <WarningOutlineIcon className={classes.warningIcon} /> },
    info: { title: 'Info', icon: <InfoOutlineIcon className={classes.infoIcon} /> },
  };

  useEffect(() => {
    if (response && status === "success" && openDlg) {
      setTimeout(() => {
        handleClose();
      }, 1200);
    }
  }, [openDlg]);

  return (
    <Dialog
      open={openDlg}
      onClose={() => handleClose()}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      fullWidth={true}
      maxWidth={maxWidth || 'xs'}
    >
      <DialogTitle id="alert-dialog-title">
        {(() => {
          // if (status === "success") return <CheckCircleOutlineIcon className={classes.successIcon} />;
          // else if (status === "error") return <ErrorOutlineIcon className={classes.errorIcon} />;
          // else if (status === "warning") return <WarningOutlineIcon className={classes.warningIcon} />;
          // else if (status === "info") return <InfoOutlineIcon className={classes.infoIcon} />;

          if (title) return (<>{textStatus[status].icon}<br />{title}</>);
          else return (<>{textStatus[status].icon}<br />{textStatus[status].title}</>);
        })()}
      </DialogTitle>

      {(() => {
        if (!isNullOrUndefined(message))
          return (
            <DialogContent>
              <DialogContentText style={{ fontSize: 14 }} id="alert-dialog-description">
                {message}
              </DialogContentText>
            </DialogContent>
          );
      })()}

      <DialogActions style={{ justifyContent: actionsAlign ? actionsAlign : "center" }} className={`${response && status === "success" ? classes.hidden : ''}`}>
        {(() => {
          if (!isNullOrUndefined(customButton)) {
            return (
              customButton.map(({ text, buttonStyle, style, onClick, autoFocus, autoClose }: CustomButton, index) => {
                return (
                  <Button
                    variant="contained"
                    className={classes[buttonStyle || 'btn_cancel']}
                    onClick={() => (onClick ? onClick() : null, handleClose(autoClose))}
                    style={style}
                    autoFocus={autoFocus || index === 0}
                  >
                    {text}
                  </Button>
                );
              })
            );
          } else if (!isNullOrUndefined(onConfirm))
            return (
              <>
                <Button
                  variant="contained"
                  className={classes.btn_delete}
                  onClick={() => { onConfirm(); handleClose(); }}
                  autoFocus
                >
                  {confirmButtonText ? confirmButtonText : 'Yes'}
                </Button>
                <Button
                  variant="contained"
                  className={classes.btn_cancel}
                  onClick={() => handleClose()}
                >
                  Cancel
                </Button>
              </>
            );
          else
            return (
              <Button
                variant="contained"
                className={classes.btn_close}
                onClick={() => handleClose()}
                autoFocus
              >
                Close
              </Button>
            );

        })()}

      </DialogActions>
    </Dialog>
  );
};
export default MsgDlg;

// ============================= Example =================================
const test = () => {
  MsgDlg({
    status: 'error',
    title: 'test',
    message: 'tttttttttt',
  });

  let response = {
    statusCode: 200,
    message: 'success',
  };

  ResultDlg(response);

  MsgDlg({
    status: 'info',
    title: 'test',
    message: 'ttttttttttttt',
    actionsAlign: "flex-end",
    customButton: [{
      onClick: () => { alert('Save'); },
      text: 'Save',
      buttonStyle: "btn_ok",
    }, {
      onClick: () => { alert("Don't Save"); },
      text: "Don't Save",
      buttonStyle: "btn_ok",
    }, {
      text: 'Close',
      buttonStyle: "btn_cancel",
      style: { marginLeft: 100 }
    }],
  });

  ConfirmDlg({
    message: 'You want to delete this item ?',
    confirmButtonText: 'Yes, delete it!',
    onConfirm: () => { alert('deleted'); },
  });
};
// ============================= Example =================================