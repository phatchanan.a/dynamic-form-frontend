import React, { CSSProperties, Dispatch, useEffect, useState } from "react";
import {
  createStyles,
  makeStyles,
  Theme,
  TextField,
  InputAdornment,
  Chip,
  Divider,
} from "@material-ui/core";
import Autocomplete from '@material-ui/lab/Autocomplete';
import SearchIcon from "@material-ui/icons/Search";
import FilterIcon from "@material-ui/icons/FilterList";

import { theme } from "../Theme/Theme";
import ChipInput from "material-ui-chip-input";
import { Column, ColumnOptional, columnsTransform } from "./DataTable";
import { isNullOrUndefined } from "util";
import { SearchViewModel } from "../model";
import { v4 as uuidv4 } from "uuid";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chipInputProps: {
      '& input': {
        marginTop: 0,
        marginBottom: 0,
        paddingBottom: 6,
      }
    },
    chipProps: {
      '& .MuiChip-deleteIcon': {
        width: 18,
      },
    },
  })
);

interface SearchProps<T = any> {
  column: Column<T>[],
  onSearch: (data: SearchData<T>[]) => void,
  conditions?: SearchData<T>[],
  filterName?: Column<T>['field'],
  setFilterName?: (name: Column<T>['field']) => void,
  style?: CSSProperties,
  hideFilter?: boolean,
  justifyStart?: boolean,
  justifyCenter?: boolean,
  justifyEnd?: boolean,
  disabled?: boolean,
};

interface SearchData<T> {
  column: keyof T | keyof ColumnOptional,
  // label: string,
  value: string,
};

export function Search<T = any>(props: SearchProps<T>) {
  const { column, onSearch, conditions, hideFilter, style, justifyStart, justifyCenter, justifyEnd, disabled, filterName, setFilterName } = props;
  const classes = useStyles();
  const [columnIndex, setColumnIndex] = useState(-1);
  const [columnFilter, setColumnFilter] = useState<Column<T>[]>([]);
  const [searchData, handleAddSearchData] = useState<SearchData<T>[]>(conditions || []);
  const [justifyContent, setJustifyContent] = useState('flex-end');
  const [chipInputID, setChipInputID] = useState('ChipInputID' + uuidv4().replace(/-/g, ""));

  useEffect(() => {
    setColumnFilter(columnsTransform(column).allColumns.filter(col => (!isNullOrUndefined(col.searching) && col.searching) || (isNullOrUndefined(col.searching))));
    // if (hideFilter && !filterName) setColumnIndex(0);

    if (justifyStart) setJustifyContent('flex-start');
    else if (justifyCenter) setJustifyContent('center');
    else if (justifyEnd) setJustifyContent('flex-end');
  }, []);

  useEffect(() => {
    handleAddSearchData(conditions);
  }, [conditions])

  useEffect(() => {
    let columnIndex = columnFilter.map(c => c.field as Column<T>['field']).indexOf(filterName);
    if (hideFilter) setColumnIndex(0);
    else setColumnIndex(columnIndex);
  }, [filterName])

  useEffect(() => {
    if (setFilterName) setFilterName(columnFilter[columnIndex]?.field || null);
  }, [columnIndex])

  return (
    <div style={Object.assign({ display: 'flex', justifyContent: justifyContent, paddingBottom: theme.spacing(0.8) }, style)}>
      <Autocomplete
        disabled={disabled}
        hidden={hideFilter}
        options={columnFilter}
        getOptionLabel={(option) => option.filterLabel ? option.filterLabel : option.label}
        value={columnIndex !== -1 ? columnFilter[columnIndex] : null}
        onChange={(e: any, data: any | null) => {
          if (data) setColumnIndex(columnFilter.indexOf(data));
          else setColumnIndex(-1);
          setTimeout(() => {
            document.getElementById(chipInputID)?.focus();
          }, 100);
        }}
        style={{ width: 250, marginRight: theme.spacing(1) }}
        // renderInput={(params) => <TextField {...params} label="Column Filter" variant="outlined" margin="dense" />}
        renderOption={(option) => (
          <div style={{ display: 'flex', marginLeft: -10 }}>
            <FilterIcon style={{ padding: 0 }} />
            <span style={{ marginTop: 0, marginLeft: 10 }}>{option.filterLabel ? option.filterLabel : option.label}</span>
          </div>
          // <Typography noWrap><Filter />{option.label}</Typography>
        )}
        renderInput={(params) => {
          return (
            <TextField
              {...params}
              label="Column filter"
              placeholder="Please select"
              margin="dense"
              variant="outlined"
              InputProps={{
                ...params.InputProps,
                startAdornment: (
                  < InputAdornment position="start" >
                    <FilterIcon />
                  </InputAdornment>
                )
              }}
            />)
        }}
      // renderTags={(tagValue, getTagProps) => {
      //   return tagValue.map((option, index) => (
      //     <Chip {...getTagProps({ index })} label={<>dddd<InputBase autoFocus /></>} />
      //   ));
      // }}
      // multiple
      />

      <ChipInput
        margin="dense"
        variant="outlined"
        allowDuplicates
        value={searchData}
        label={
          <div style={{ display: 'flex', marginTop: -4, marginLeft: -4 }}>
            <SearchIcon style={{ padding: 0 }} />
            <span style={{ marginTop: 4, marginLeft: 4 }}>{hideFilter ? (columnFilter[columnIndex]?.filterLabel || columnFilter[columnIndex]?.label) : 'Search'}</span>
          </div>
        }
        chipRenderer={({ value, text, chip, isFocused, isDisabled, isReadOnly, handleClick, handleDelete, className }, key) => {
          let label = columnFilter.filter(col => col.field === chip.column)[0]?.filterLabel || columnFilter.filter(col => col.field === chip.column)[0]?.label;
          return (
            <Chip
              onDelete={handleDelete}
              key={key}
              clickable
              className={`${className} ${classes.chipProps}`}
              label={
                <>
                  {label && !hideFilter && <b>{label}<Divider orientation="vertical" style={{ display: 'inline-flex', height: 20, margin: '-3px 4px' }} /></b>}
                  {chip.value}
                </>
              }
              style={{ height: 25, margin: '0px 8px 3px -3px', fontSize: 'inherit', color: theme.palette.grey[600] }}
              avatar={<SearchIcon style={{ width: 18 }} />}
            />
          );
        }}
        onAdd={(value) => {
          let data = [...searchData, { column: columnFilter[columnIndex].field, value: value }];
          handleAddSearchData(data);
          onSearch(data);
        }}
        onDelete={(text, index) => {
          let data = [...searchData.filter((d, i) => i !== index)];
          handleAddSearchData(data);
          onSearch(data);
        }}
        onKeyUp={(event) => {
          // console.log(event.keyCode);
          // console.log((event.target as HTMLInputElement).value);
          if (event.keyCode === 8 && searchData.length !== 0 && !(event.target as HTMLInputElement).value) {
            let data = [...searchData.filter((d, i) => i !== searchData.length - 1)];
            handleAddSearchData(data);
            onSearch(data);
          }
        }}

        disabled={columnIndex === -1 || disabled}
        className={classes.chipInputProps}
        InputLabelProps={{
          style: { marginTop: 0, top: 0 }
        }}
        InputProps={{
          id: chipInputID,
          style: { padding: '6px 0px 2px 10px' }
        }}
        style={{ width: 800 }}
      />

    </div>
  );
};

export const initSearchParams = <T extends {} = any>(v: SearchViewModel<T>): SearchViewModel<T> => {
  let order: SearchViewModel<T>;
  if (typeof v.order === "object")
    order = {
      orderBy: Object.keys(v.order)[0] as keyof T,
      direction: v.order[Object.keys(v.order)[0]],
    }

  return Object.assign({
    page: 1,
    limit: 25,
    conditions: '[]',
    // orderBy: v.orderBy,
    // direction: 'asc',
  }, order, v);
}
