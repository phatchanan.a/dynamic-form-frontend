import React, { CSSProperties } from "react";
import {
  Typography
} from "@material-ui/core";
import { Pagination as MuiPagination } from "@material-ui/lab";
import { theme } from "../Theme/Theme";
import { PaginationViewModel } from "../model";
import { v4 as uuidv4 } from "uuid";

interface Props {
  paging: PaginationViewModel,
  onPageChange: (event, page) => void,
  style?: CSSProperties,
};

export function Pagination(props: Props) {
  const { paging, onPageChange, style } = props;
  // const [id, setId] = useState('Pagination' + uuidv4().replace(/-/g, ""));

  return (
    <div id={('Pagination' + uuidv4().replace(/-/g, ""))} style={Object.assign({
      padding: theme.spacing(0.6, 1), justifyContent: 'flex-end',
      alignItems: 'center', display: 'flex', color: theme.palette.text.primary,
      boxShadow: '0 -8px 10px -5px rgba(0, 0, 0, 0.20)',
    }, style)} >
      <Typography>
        Items : &nbsp;
            {(paging?.currentPage - 1) * paging?.itemsPerPage + (paging?.itemCount > 0 ? 1 : 0)}-
            {(paging?.currentPage - 1) * paging?.itemsPerPage + paging?.itemCount} of {paging?.totalItems}
      </Typography>
      <MuiPagination
        // variant="outlined"
        // shape="rounded"
        showFirstButton
        showLastButton
        count={paging?.totalPages}
        page={paging?.currentPage || 1}
        onChange={onPageChange}
      />
    </div>
  );
};

