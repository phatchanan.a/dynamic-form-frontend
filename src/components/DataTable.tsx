import React, { useEffect, Dispatch, CSSProperties } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell, { TableCellProps } from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import { SearchViewModel, PaginationViewModel } from '../model';
import { isNullOrUndefined } from 'util';
// import FirstPageIcon from '@material-ui/icons/FirstPage';
// import LastPageIcon from '@material-ui/icons/LastPage';
// import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
// import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { Pagination } from '@material-ui/lab';

export function columnsTransform<T>(columns, genRowsHeader = false): { allColumns: Column<T>[], allRowsOfColumns?: Column<T>[][] } {
  let allColumns: Column<T>[] = [];
  let allRowsOfColumns: Column<T>[][] = [columns];

  columns.forEach((lv1, i) => {
    if (!lv1.subColumn) allColumns.push(lv1);
    else {
      (lv1.subColumn as Column<T>[]).forEach((lv2, j) => {
        if (!lv2.subColumn) {
          allColumns.push(lv2);
          if (genRowsHeader) {
            if (!allRowsOfColumns[1]) allRowsOfColumns.push([lv2])
            else allRowsOfColumns[1].push(lv2)
          }
        }
        else {
          (lv2.subColumn as Column<T>[]).forEach((lv3, k) => {
            allColumns.push(lv3);
            if (genRowsHeader) {
              if (!allRowsOfColumns[2]) allRowsOfColumns.push([lv3])
              else allRowsOfColumns[2].push(lv3)
            }
          });
        }
      });
    }
  });
  if (genRowsHeader) return { allColumns, allRowsOfColumns }
  else return { allColumns }
}

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Direction = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  direction: Direction,
  orderBy: Key,
): (a: { [key in Key]: any }, b: { [key in Key]: any }) => number {
  return direction === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: any, b: any) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

export interface ColumnOptional {
  Action?: string
}

export interface Column<T> {
  field?: keyof T | keyof ColumnOptional;
  filterLabel?: string,
  label: string;
  headerProps?: TableCellProps;
  bodyProps?: TableCellProps;
  searching?: boolean;
  ordering?: boolean;
  render?: (v: T) => JSX.Element;
  subColumn?: Column<T>[] | TableCellProps[];
}

interface EnhancedTableProps<T extends ColumnOptional> {
  classes: ReturnType<typeof useStyles>;
  numSelected?: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof T) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  direction: Direction;
  orderBy?: keyof T | string;
  rowCount: number;
  columns: Column<T>[][];
  hiddenColumn?: (keyof T)[];
  headerStyle?: CSSProperties | {},
}

function EnhancedTableHead<T extends ColumnOptional>(props: EnhancedTableProps<T>) {
  const { classes, onSelectAllClick, direction, orderBy, numSelected, rowCount, onRequestSort, columns, hiddenColumn, headerStyle } = props;
  const createSortHandler = (property: keyof T | keyof ColumnOptional) => (event: React.MouseEvent<unknown>) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      {columns.map((rowColumn, headerIndex) =>

        (rowColumn.length > 0) &&
        <TableRow key={headerIndex}>
          {(() => {
            if (numSelected !== undefined) return (
              headerIndex === 0 &&
              <TableCell padding="checkbox" style={Object.assign({}, headerStyle)}>
                <Checkbox
                  indeterminate={numSelected > 0 && numSelected < rowCount}
                  checked={rowCount > 0 && numSelected === rowCount}
                  onChange={onSelectAllClick}
                  inputProps={{ 'aria-label': 'select all desserts' }}
                />
              </TableCell>
            );
            else return (
              headerIndex === 0 &&
              <TableCell align="center" style={Object.assign({ width: 30 }, headerStyle)} rowSpan={columns.length || 1}>
                <div style={{ padding: 9 }}>No.</div>
              </TableCell>
            );
          })()}

          {rowColumn.filter(e => hiddenColumn ? hiddenColumn.filter(h => h === e.field).length === 0 : true).map((headCell) => (
            <TableCell
              {...{ ...headCell.headerProps, style: { ...headerStyle, ...headCell.headerProps?.style } }}
              key={String(headCell.field)}
              sortDirection={orderBy === headCell.field ? direction : false}
            >
              <TableSortLabel
                disabled={(!isNullOrUndefined(headCell.ordering) && !headCell.ordering) || !headCell.field}
                color="#fff"
                active={orderBy === headCell.field}
                direction={orderBy === headCell.field ? direction : 'asc'}
                onClick={createSortHandler(headCell.field)}
              >
                {headCell.label}
                {orderBy === headCell.field ? (
                  <span className={classes.visuallyHidden}>
                    {direction === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>

      )}
    </TableHead>
  );
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      '& th .MuiTableSortLabel-icon': {
        position: 'absolute',
        right: -25,
        color: "#FAFAFA !important",
      }
    },
    table: {

      minWidth: 750,
      '& td': {
        padding: theme.spacing(0.3, 2),
        cursor: 'pointer',
      },
      '& th': {
        // textAlignLast: 'center',
        position: 'relative',
        // backgroundColor: theme.palette.primary.main,
        color: "#FAFAFA !important",

        fontSize: '16px !important',
        // fontWeight: 'bold !important',
        whiteSpace: 'nowrap !important',
        padding: theme.spacing(0.3, 2),
        '& .MuiTableSortLabel-icon': {
          color: "#FAFAFA !important",
        },
        '& .MuiTableSortLabel-active': {
          color: "#FAFAFA !important",
          //textShadow: '1px 1px 0px rgba(255, 255, 255, 0.3)',
          fontWeight: 'bolder',
        },
        '& .MuiTableSortLabel-root:hover ': {
          color: '#FAFAFA !important '
        }
      },
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }),
);

interface TableProps<T extends ColumnOptional> extends Partial<Pick<
  { disabledClearSelected?: boolean },
  'disabledClearSelected'>> {
  data: T[],
  selected?: string[],
  setSelected?: Dispatch<string[]>,
  columnSelected?: keyof T,
  columns: Column<T>[],
  hiddenColumn?: (keyof T)[],
  orderBy?: keyof T,
  limit?: number,
  direction?: keyof { 'asc', 'desc' },
  paging?: PaginationViewModel,
  searchParams?: SearchViewModel<T>,
  onRender?: (data: SearchViewModel<T>) => void,
  headerStyle?: CSSProperties | {},
  bodyStyle?: CSSProperties | {},
};

export default function DataTable<T>(props: TableProps<T>) {
  const { data, selected, setSelected, columnSelected, columns, hiddenColumn, searchParams, onRender, paging, limit, disabledClearSelected, headerStyle, bodyStyle } = props;
  const classes = useStyles();
  const [direction, setDirection] = React.useState<Direction>();
  const [orderBy, setOrderBy] = React.useState<keyof T>();
  // const [selected, setSelected] = React.useState<string[]>([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(limit || 25);
  const [rows, setRows] = React.useState<any[]>([]);

  const [allColumns, setAllColumns] = React.useState<Column<T>[]>([]);
  const [allRowsOfColumns, setAllRowsOfColumns] = React.useState<Column<T>[][]>([]);

  useEffect(() => {
    setRows(data || []);
  }, [data]); // eslint-disable-line react-hooks/exhaustive-deps  

  useEffect(() => {
    setDirection((searchParams ? searchParams.direction : props.direction) || 'asc');
    setOrderBy((searchParams ? searchParams.orderBy : props.orderBy));
    setRowsPerPage((searchParams ? searchParams.limit : 25));
  }, [searchParams]); // eslint-disable-line react-hooks/exhaustive-deps  

  useEffect(() => {
    if (setSelected && !disabledClearSelected) setSelected([]);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps  

  useEffect(() => {
    let { allColumns, allRowsOfColumns } = columnsTransform<T>(columns, true);

    setAllColumns(allColumns);
    setAllRowsOfColumns(allRowsOfColumns);
  }, [columns]); // eslint-disable-line react-hooks/exhaustive-deps  

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof T) => {
    const isAsc = orderBy === property && direction === 'asc';
    setDirection(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
    if (onRender) onRender({ ...searchParams, orderBy: property, direction: isAsc ? 'desc' : 'asc' });
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!columnSelected) return;
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => String(n[columnSelected]));
      if (setSelected) setSelected(newSelecteds);
      return;
    }
    if (setSelected) setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    if (!selected) return;
    const selectedIndex = selected.indexOf(name);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    if (setSelected) setSelected(newSelected);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
    if (onRender) onRender({ ...searchParams, page: newPage + 1 });
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
    if (onRender) onRender({ ...searchParams, page: 1, limit: parseInt(event.target.value, 10) });
  };

  const isSelected = (name: string) => selected?.indexOf(name) !== -1 && selected !== undefined;

  // const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const dataRows = (isNullOrUndefined(paging) ? stableSort(rows, getComparator(direction, orderBy)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage) : rows);

  return (
    <div className={classes.root}>
      {/* <pre>
        <code>{JSON.stringify(dataRows.map(e => e.FormName), null, 2)}</code>
      </pre> */}
      <TableContainer component={Paper} elevation={4} style={{ width: '100%' }}>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          // size={dense ? 'small' : 'medium'}
          aria-label="enhanced table"
        >
          <EnhancedTableHead<T>
            columns={allRowsOfColumns}
            hiddenColumn={hiddenColumn}
            classes={classes}
            numSelected={selected?.length}
            direction={direction}
            orderBy={orderBy}
            onSelectAllClick={handleSelectAllClick}
            onRequestSort={handleRequestSort}
            rowCount={rows.length}
            headerStyle={headerStyle}
          />
          <TableBody>
            {
              //stableSort(rows, getComparator(direction, orderBy))
              // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              dataRows.map((row, index) => {
                const isItemSelected = columnSelected ? isSelected(String(row[columnSelected])) : false;
                const labelId = `enhanced-table-checkbox-${index}`;

                return (
                  <TableRow
                    hover
                    onClick={(event) => columnSelected ? handleClick(event, String(row[columnSelected])) : null}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={String(columnSelected ? `${row[columnSelected]}_${index}` : `_${index}_`)}
                    selected={isItemSelected}
                  >
                    {(() => {
                      if (setSelected !== undefined) return (
                        <TableCell padding="checkbox" style={Object.assign({}, bodyStyle)}>
                          <Checkbox checked={isItemSelected} inputProps={{ 'aria-labelledby': labelId }} />
                        </TableCell>
                      );
                      else return (<TableCell align="center" style={Object.assign({}, bodyStyle)}>
                        <div style={{ padding: 9 }}>{(page * rowsPerPage) + (index + 1)}</div>
                      </TableCell>);
                    })()}


                    {allColumns.filter(e => hiddenColumn ? hiddenColumn.filter(h => h === e.field).length === 0 : true).map((cell, i) => (
                      <TableCell
                        key={String(cell.field)}
                        {...cell.bodyProps ? { ...cell.bodyProps, style: { ...bodyStyle, ...cell.bodyProps?.style } } :
                          { ...cell.headerProps, rowSpan: 1, colSpan: 1, style: { ...bodyStyle, ...cell.headerProps?.style } }
                        }
                      >
                        {cell.render ? cell.render(row) : row[cell.field]}
                      </TableCell>
                    ))}

                  </TableRow>
                );
              })}
            {(() => {
              if (rows.length === 0) return (
                <TableRow>
                  <TableCell align="center" colSpan={allColumns.length + 1}><div style={{ padding: 9 }}>No Records to display</div></TableCell>
                </TableRow>
              );
            })()}
          </TableBody>
        </Table>
        <TablePagination
          // style={{ boxShadow: '0 -8px 10px -5px rgba(0, 0, 0, 0.20)' }}
          rowsPerPageOptions={[25, 50, 100, { label: 'All', value: (paging ? (paging?.totalItems || 0) : rows.length) }]}
          component="div"
          // count={rows.length}
          count={paging ? (paging?.totalItems || 0) : rows.length}
          // rowsPerPage={rowsPerPage}
          rowsPerPage={paging ? (paging?.itemsPerPage || 25) : rowsPerPage}
          // page={page}
          page={paging ? ((paging?.currentPage || 1) - 1) : page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={(paging ? (paging?.totalItems || 0) : rows.length) !== 0 ? handleChangeRowsPerPage : null}
          ActionsComponent={TablePaginationActions}
        />
      </TableContainer>
    </div>
  );
}

//==========================================================================================

const pagingStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexShrink: 0,
      marginLeft: theme.spacing(2.5),
    },
  }),
);

interface TablePaginationActionsProps {
  count: number;
  page: number;
  rowsPerPage: number;
  onChangePage: (event: React.MouseEvent<HTMLButtonElement>, newPage: number) => void;
}

function TablePaginationActions(props: TablePaginationActionsProps) {
  const classes = pagingStyles();
  const { count, page, rowsPerPage, onChangePage } = props;
  const totalPage = Math.max(0, Math.ceil(count / rowsPerPage));

  // const handleFirstPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
  //   onChangePage(event, 0);
  // };

  // const handleBackButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
  //   onChangePage(event, page - 1);
  // };

  // const handleNextButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
  //   onChangePage(event, page + 1);
  // };

  // const handleLastPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
  //   onChangePage(event, totalPage);
  // };

  return (
    <div className={classes.root}>
      {/* <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton> */}

      <Pagination
        // variant="outlined"
        // shape="rounded"
        showFirstButton
        showLastButton
        count={totalPage}
        page={page + 1}
        onChange={(event, page) => onChangePage(null, page - 1)}
      />

      {/* <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton> */}
    </div>
  );
}