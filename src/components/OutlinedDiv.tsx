import React from "react";
import {
  TextField,
  createStyles,
  makeStyles,
  Theme,
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    OutlineDiv: {
      margin: theme.spacing(1, 0, 0.5, 0),
      cursor: 'hand !important' as any,
      // padding: theme.spacing(1),
      // borderRadius: 5,
      // border: "1px #ddd solid",
    },
    labelRoot: {
      // fontSize: '1.2em',
      // color: "red",
      // "&$labelFocused": {
      //   color: "purple"
      // }
    },
    labelFocused: {},
  })
);

const InputComponent = ({ inputRef, ...other }: any) => <div {...other} />;
const OutlinedDiv = ({ children, label, error, required }: any) => {
  const classes = useStyles();

  return (
    <TextField
      className={`${classes.OutlineDiv}`}
      variant="outlined"
      required={required}
      error={error}
      label={label}
      multiline
      // InputLabelProps={{ shrink: true }}
      InputLabelProps={{
        shrink: true,
        classes: {
          root: classes.labelRoot,
          focused: classes.labelFocused,
        }
      }}
      InputProps={{
        inputComponent: InputComponent,
      }}
      inputProps={{ children: children }}
    />
  );
};
export default OutlinedDiv;