import React, { useState } from 'react';
import {
  withStyles,
  WithStyles,
} from '@material-ui/core/styles';
import {
  makeStyles,
  createStyles,
  Theme,
  Button,
  Typography,
  ThemeProvider
} from "@material-ui/core";
import MuiDialog, { DialogProps } from "@material-ui/core/Dialog";
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { CSSProperties } from '@material-ui/core/styles/withStyles';
import { isNullOrUndefined } from 'util';
import Scrollbar from 'smooth-scrollbar-react';
import { theme } from '../Theme/Theme';
import ReactDOM from 'react-dom';
import { v4 as uuidv4 } from "uuid";

const useStyles = makeStyles((theme: Theme) => ({

  //================== Button =====================
  btn_cancel: {
    background: theme.palette.grey[400],
    color: '#000',
    "&:hover": {
      background: theme.palette.grey[500],
    },
  },
  btn_delete: {
    background: theme.palette.error.main,
    color: "#FAFAFA",
    "&:hover": {
      background: theme.palette.error.dark,
    },
  },
  btn_ok: {
    background: theme.palette.primary.main,
    color: "#FAFAFA",
    "&:hover": {
      background: theme.palette.primary.dark,
    },
  },
  btn_close: {
    background: theme.palette.primary.main,
    color: "#FAFAFA",
    "&:hover": {
      background: theme.palette.primary.dark,
    },
  },
}));

//=======================================================================================================

export const Dialog = withStyles((theme: Theme) => ({
  root: {
    '& .MuiDialog-container': {
      zIndex: 888,
    },
    // '& .MuiDialog-paper': {
    //   padding: theme.spacing(1),
    //   justifyContent: 'center',
    //   minHeight: '30%',
    // },
  },
}))(MuiDialog);

const DialogTitleStyles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      background: '#ddd',
      // height: theme.spacing(3),
      padding: theme.spacing(1, 2),
      // paddingTop: theme.spacing(1),
      boxShadow: 'inset 0px 0 10px 0px rgba(0, 0, 0, 0.20)',
      borderBottom: '1px #909090 solid',
      '& h6': {
        fontWeight: 'bold',
      },
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(0.5),
      top: theme.spacing(0.5),
      color: theme.palette.grey[500],
      padding: 8,
      fontSize: '0.5rem',
    },
  });

export interface DialogTitleProps extends WithStyles<typeof DialogTitleStyles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
  image?: boolean;
}

export const DialogTitle = withStyles(DialogTitleStyles)((props: DialogTitleProps) => {
  const { children, classes, onClose, image, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={!image ? classes.root : ''} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} style={{ padding: !image ? 8 : 2 }} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

export const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(0),
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    // height: '70vh',
    minHeight: '30vh',
  },
}))(MuiDialogContent);

export const DialogActions = withStyles((theme: Theme) => ({
  root: {
    boxShadow: '0 -8px 10px -5px rgba(0, 0, 0, 0.20)',
    padding: theme.spacing(2),
    '& .MuiButton-root:not(:first-child)': {
      marginLeft: theme.spacing(1.5),
    },
  },
}))(MuiDialogActions);

//=======================================================================================================

interface CustomButton {
  text?: string,
  onClick?: () => void,
  style?: CSSProperties,
  autoFocus?: boolean,
  autoClose?: boolean,
  disabled?: boolean,
  buttonStyle?: keyof { 'btn_ok', 'btn_delete', 'btn_close', 'btn_cancel' },
}

export interface CustomizedDialogProps {
  image?: boolean,
  title?: string,
  openDlg?: boolean,
  children?: any,
  setOpenDlg?: (e) => void,
  onSubmit?: CustomButton,
  onClose?: () => void,
  maxWidth?: DialogProps['maxWidth'],
  customButton?: CustomButton[],
  actionsAlign?: keyof { 'flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'initial', 'inherit' },
  customActions?: React.ReactNode,
}

//=====================================================================================================================================

export const CusDlg = (props: CustomizedDialogProps) => {
  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <CustomizedDialogs {...props} key={uuidv4()} />
    </ThemeProvider>,
    document.getElementById('CusDlg'));
};

export default function CustomizedDialogs(props: CustomizedDialogProps) {
  const { title, onSubmit, onClose, children, maxWidth, customButton, actionsAlign, customActions, image } = props;
  const classes = useStyles();
  const [openDlg, setOpenDlg] = useState(true);

  const handleClose = (autoClose: boolean = true) => {
    if (onClose) onClose();
    if (!isNullOrUndefined(autoClose) && autoClose) {
      setOpenDlg(false);
      if (props.setOpenDlg) props.setOpenDlg(false);
    }
  };

  return (<>
    {!image &&
      <Dialog
        open={openDlg}
        onClose={() => handleClose()}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
        maxWidth={maxWidth || 'md'}
        fullWidth
      >
        <DialogTitle id="customized-dialog-title" onClose={() => handleClose()}>
          {title}
        </DialogTitle>

        <DialogContent dividers>
          <Scrollbar>
            <div>
              <div style={{ padding: theme.spacing(2), minHeight: '30vh' }}>{children}</div>
            </div>
          </Scrollbar>
        </DialogContent>

        {/* <Divider /> */}
        <DialogActions style={{ justifyContent: actionsAlign ? actionsAlign : "flex-end" }}>
          {(() => {
            if (customActions) return (customActions);
            else if (!isNullOrUndefined(customButton)) {
              return (
                customButton.map(({ text, buttonStyle, style, onClick, autoFocus, autoClose, disabled }: CustomButton, index) => {
                  return (
                    <Button
                      variant="contained"
                      className={classes[buttonStyle || 'btn_cancel']}
                      onClick={() => {
                        if (onClick) onClick();
                        handleClose(autoClose)
                      }}
                      style={style}
                      autoFocus={autoFocus || index === 0}
                      disabled={disabled || false || !openDlg}
                    >
                      {text}
                    </Button>
                  );
                })
              );
            } else if (!isNullOrUndefined(onSubmit))
              return (
                <>
                  <Button
                    variant="contained"
                    className={classes.btn_ok}
                    onClick={() => {
                      if (onSubmit.onClick) onSubmit.onClick();
                      handleClose(onSubmit.autoClose)
                    }}
                    disabled={onSubmit.disabled || false || !openDlg}
                    autoFocus
                  >
                    {onSubmit.text ? onSubmit.text : 'OK'}
                  </Button>
                  <Button
                    variant="contained"
                    className={classes.btn_cancel}
                    onClick={() => handleClose()}
                  >
                    CANCEL
                </Button>
                </>
              );
            else
              return (
                <Button
                  variant="contained"
                  className={classes.btn_close}
                  onClick={() => handleClose()}
                  autoFocus
                >
                  Close
                </Button>
              );

          })()}

        </DialogActions>
      </Dialog>}

    {image &&
      <Dialog
        open={openDlg}
        onClose={() => handleClose()}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
        maxWidth={maxWidth || 'md'}
      // fullWidth
      >
        <DialogTitle image id="customized-dialog-title" onClose={() => handleClose()}>
          {/* {title} */}
        </DialogTitle>

        <DialogContent style={{ padding: theme.spacing(0.5) }}>
          {children}
        </DialogContent>

      </Dialog>}
  </>
  );
}