import React from "react";
import {
  Paper,
  Theme,
  makeStyles,
  createStyles,
} from "@material-ui/core";
import { theme } from "../Theme/Theme";

export interface LayoutProps {
  className?: string;
  children?: React.ReactNode;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // height: "92vh",
      display: "flex",
      flex: 1,
      flexDirection: "column",
      '& > .MuiPaper-root:nth-child(1)': {
        marginBottom: '0px !important',
      }
    },
    paper: {
      padding: theme.spacing(2),
      color: theme.palette.text.secondary,
      display: "flex",
      alignItems: "center",
    },
    header: {
      minHeight: `5vh`,
      margin: theme.spacing(1.5),
    },
    body: {
      flexDirection: 'column',
      margin: theme.spacing(1.5),
      // minHeight: `calc(100vh - (8vh + ${theme.spacing(1.5)}px*3) - 8vh - (${theme.spacing(4)}px*2))`,
    }
  })
);

export const AppLayout = (props: LayoutProps | any) => {
  const classes = useStyles();

  //set auto height for full height layout body.
  // if (document.getElementById('layout-body'))
  //   document.getElementById('layout-body').style.minHeight = `calc(100vh 
  //     - ${document.querySelector('#layout-header-bar').clientHeight}px 
  //     - ${document.querySelector('#layout-header').clientHeight}px 
  //     - (${theme.spacing(1.5)}px*3)
  //     - (${theme.spacing(4)}px))`;
  if (document.getElementById('layout-body'))
    document.getElementById('layout-body').style.minHeight = `calc(92vh 
      - ${document.querySelector('#layout-header-bar').clientHeight}px 
      - ${document.querySelector('#layout-header').clientHeight}px 
      - (${theme.spacing(1.5)}px))`;

  return (
    <div className={`${classes.root} ${props.className}`}>

      {(() => {
        // support with tag <> and <Paper>
        if (String(props.children[0].type) === 'Symbol(react.fragment)' || String(props.children[1].type?.options?.name) === 'MuiPaper')
          return (
            <Paper id="layout-header" className={`${classes.paper} ${classes.header} ${props.children[0].className}`} elevation={5} style={props.children[0].props.style}>
              {props.children[0].props.children}
            </Paper>
          );
        else {
          return (
            <div id="layout-header" className={`${classes.header}`}>
              {props.children[0]}
            </div>
          );
        }
      })()}

      {(() => {
        // support with tag <> and <Paper>
        if (String(props.children[1].type) === 'Symbol(react.fragment)' || String(props.children[1].type?.options?.name) === 'MuiPaper')
          return (
            <Paper id="layout-body" className={`${classes.paper} ${classes.body} ${props.children[1].className}`} elevation={5} style={props.children[1].props.style}>
              {props.children[1].props.children}
            </Paper>
          );
        else {
          return (
            <div id="layout-body" className={`${classes.body}`}>
              {props.children[1]}
            </div>
          );
        }
      })()}

      {/* Other element in this AppLayout */}
      {(props.children as []).map((e, i) => {
        if (i >= 2) {
          return (e);
        }
      })}
    </div>
  );
};