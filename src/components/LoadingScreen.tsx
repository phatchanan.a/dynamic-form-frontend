import React from "react";
import ReactDOM from "react-dom";
import {
  makeStyles,
  Theme,
  ThemeProvider,
  Fade,
} from "@material-ui/core";
// import ClipLoader from "react-spinners/ClipLoader";
import GridLoader from "react-spinners/GridLoader";
import { theme } from "../Theme/Theme";

const useStyles = makeStyles((theme: Theme) => ({
  loadingScreenWrapper: {
    opacity: 1,
    position: 'absolute',
    borderRadius: 'inherit',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    minHeight: '100vh',
    minWidth: '100vw',
    overflow: 'hidden',
    zIndex: 9999,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    visibility: 'visible',
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    '& div': {
      //   background: 'transparent !important',
      //   width: '150px',
      //   height: '150px',
      //   borderRadius: '100%',
      //   border: '15px solid',
      //   borderColor: '#123abc',
      //   borderBottomColor: 'transparent',
      //   display: 'inline-block',
      // webkitAnimation: 'animation-s8tf20 0.75s 0s infinite linear',
      // animation: 'animation-s8tf20 0.75s 0s infinite linear',
      // webkitAnimationFillMode: 'both',
      // animationFillMode: 'both',

      // animationDuration: '1200ms',
      // WebkitFilter: 'drop-shadow( 0px 0px 5px rgba(0, 0, 0, 0.5))',
      // filter: 'drop-shadow( 0px 0px 5px rgba(0, 0, 0, 0.5))',
      // WebkitBackdropFilter: 'drop-shadow( 0px 0px 5px rgba(0, 0, 0, 0.5))',

      WebkitFilter: 'drop-shadow( 10px 10px 4px rgba(0, 0, 0, 0.2))',
      filter: 'drop-shadow( 10px 10px 4px rgba(0, 0, 0, 0.2))',
      WebkitBackdropFilter: 'drop-shadow( 10px 10px 4px rgba(0, 0, 0, 0.2))',
    }
  },

  // spinCircle: {
  //   top: '50%',
  //   left: '50%',

  //   borderTopColor: '#3366ff',
  //   borderRightColor: 'transparent',
  //   borderBottomColor: '#3366ff',
  //   borderLeftColor: '#3366ff',

  //   animation: 'spin 0.8s infinite linear',
  //   borderRadius: '50%',
  //   borderStyle: 'solid',
  //   borderWidth: '1em !important',
  //   width: '8em !important',
  //   height: '8em !important',
  // },

}));

//===============================================================================

interface LoadingProps {

};

//===============================================================================

export const ShowLoadingScreen = (show: boolean = true, props?: LoadingProps) => {
  if (document.getElementById('Loading'))
    ReactDOM.render(
      <ThemeProvider theme={theme}>
        <CustomLoading {...props} show={show} />
      </ThemeProvider>,
      document.getElementById('Loading'));
};

//===============================================================================

const CustomLoading = (props: any) => {
  const { show } = props;
  const classes = useStyles();

  return (
    // <Fade in={show} timeout={300}>
    //   <div className={`${classes.loadingScreenWrapper}`} >
    //     <ClipLoader
    //       size={150}
    //       color={"#123abc"}
    //       loading={show}
    //     />
    //   </div>
    // </Fade>
    <Fade in={show} timeout={500}>
      <div className={`${classes.loadingScreenWrapper}`} >
        <GridLoader
          size={30}
          margin={5}
          color={theme.palette.primary.main}
          loading={show}
        />
      </div>
    </Fade>
  );

  // return (
  //   <Modal open={show}   >

  //     <div className={`${classes.loadingScreenWrapper}`} >
  //       <CircularProgress disableShrink thickness={5} size={150} style={{
  //         // animationDuration: '800ms',
  //         WebkitFilter: 'drop-shadow( 0px 0px 5px rgba(0, 0, 0, 0.5))',
  //         filter: 'drop-shadow( 0px 0px 5px rgba(0, 0, 0, 0.5))',
  //         WebkitBackdropFilter: 'drop-shadow( 0px 0px 5px rgba(0, 0, 0, 0.5))',
  //       }} />
  //     </div>
  //   </Modal>
  //   // <Fade in={show} timeout={100}></Fade>
  // );
};

export default ShowLoadingScreen;

