import React, { FunctionComponent, useState } from "react";
import {
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  List,
  makeStyles,
  Theme,
  createStyles,
} from "@material-ui/core";
import { ExpandMore, ExpandLess } from "@material-ui/icons";
import { FormDetails, PropsExpand } from "../model/form";
import { useDispatch, useSelector } from "react-redux";
import * as fieldsAction from "../actions/fields.action";
import * as propertiesAction from "../actions/properties.action";
import { Scrollbar_ } from "../screens/header";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listItem: {
      padding: theme.spacing(0.8, 1, 0.8, 4),
    },
    mainItem: {
      padding: theme.spacing(0.3, 0.8),
      fontSize: 16,
    },
    itemText: {
      // fontSize: 14,
    },
  })
);

let seq = 0;

const Expand: FunctionComponent<PropsExpand> = (props: PropsExpand) => {
  const dispatch = useDispatch();
  const { title, formDetails } = props;
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const handleClick = () => {
    setOpen(!open);
  };

  const fieldsReducer = useSelector(({ fieldsReducer }: any) => fieldsReducer);
  const { present } = fieldsReducer;

  const handleAddToContent = (item: FormDetails, i: number) => {
    seq = Math.max.apply(Math, present.content_data.map(o => { return o.SequenceNo; }));
    item.SequenceNo = (seq < 0) ? 0 : seq + 1;
    dispatch(fieldsAction.addFields(item));
    if (item.FieldKey !== "page_break") dispatch(propertiesAction.addProperties(present.content_data.length));

    const container: any = document.querySelector("#Content");
    setTimeout(() => {
      Scrollbar_.get(container).update();
      Scrollbar_.get(container).scrollTop = container.scrollHeight + container.scrollTop;
    }, 500);
  };

  return (
    <>
      <ListItem button onClick={handleClick} className={classes.mainItem}>
        <ListItemText primary={title} classes={{ primary: classes.mainItem }} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {formDetails?.filter(fd => fd.FieldKey !== "punch_remark").map((item, i) => {
            return (
              <ListItem
                key={i}
                button
                disabled={item.FieldKey === "punch_list" && present.content_data.filter(data => data.FieldKey === "punch_list").length >= 1}
                className={classes.listItem}
                onClick={() => {
                  if (item.FieldKey === "punch_list") {
                    handleAddToContent(item, i);
                    handleAddToContent(formDetails.filter(fd => fd.FieldKey === "punch_remark")[0], i);
                  } else {
                    handleAddToContent(item, i);
                  }
                }}
              >
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText
                  primary={item.FieldType}
                  classes={{ primary: classes.itemText }}
                />
              </ListItem>
            );
          })}
        </List>
      </Collapse>
    </>
  );
};

export default Expand;
