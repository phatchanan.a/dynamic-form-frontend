import React from "react";
import {
  List,
  AppBar,
  Toolbar,
  Typography,
  makeStyles,
  Theme,
  createStyles,
  AppBarProps,
} from "@material-ui/core";
import UndoRedoContent from "./UndoRedoContent";

export interface Props {
  children?: React.ReactNode;
  title: string;
  appBarColor?: AppBarProps["color"];
  link?: JSX.Element;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // flexGrow: 1,
      // paddingBottom: theme.spacing(2),
      // height: "100vh",
      width: '-webkit-fill-available',
      paddingBottom: 0,
    },
    subheader: {
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      padding: theme.spacing(0.5, 1.5),
      display: '-webkit-inline-box',
    },
    link: {
      position: 'absolute',
      right: 4,
    },
  })
);

const Layout = (props: Props) => {
  const classes = useStyles();
  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <AppBar id="formLayoutHeader" className={classes.subheader} position="static" color={props.appBarColor}>
          {/* <Toolbar > */}
          <Typography variant="h6" color="inherit" style={{ fontSize: '1.1rem', lineHeight: 2 }}>
            {props.title}
          </Typography>
          <UndoRedoContent title={props.title} />
          {/* </Toolbar> */}
          <div className={classes.link}>{props.link}</div>
        </AppBar>
      }
      className={classes.root}
    >
      {props.children}
    </List>
  );
};

export default Layout;
