import React from "react";
import { ActionCreators as UndoActionCreators } from "redux-undo";
import { connect } from "react-redux";
import { IconButton, makeStyles, Theme, createStyles } from "@material-ui/core";
import UndoIcon from "@material-ui/icons/Undo";
import RedoIcon from "@material-ui/icons/Redo";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    undoButton: {
      marginLeft: theme.spacing(2),
      padding: theme.spacing(0.75),
    },
    redoButton: {
      padding: theme.spacing(0.75),
    },
  })
);

let UndoRedoContent: React.FC<any> = ({ canUndo, canRedo, onUndo, onRedo, title }: any) => {
  const classes = useStyles();
  return (
    'Content' !== title ? <></> :
      <>
        <IconButton
          onClick={onUndo}
          disabled={!canUndo}
          className={classes.undoButton}
          aria-label="search"
          edge="start"
          color="inherit"
        >
          <UndoIcon fontSize="small" />
        </IconButton>

        <IconButton
          onClick={onRedo}
          disabled={!canRedo}
          className={classes.redoButton}
          color="inherit"
          aria-label="open drawer"
        >
          <RedoIcon fontSize="small" />
        </IconButton>
      </>
  );
};

const mapStateToProps = (state: any) => ({
  canUndo: state.fieldsReducer.past.length > 0,
  canRedo: state.fieldsReducer.future.length > 0,
});

const mapDispatchToProps = {
  onUndo: UndoActionCreators.undo,
  onRedo: UndoActionCreators.redo,
};

UndoRedoContent = connect(mapStateToProps, mapDispatchToProps)(UndoRedoContent);

export default UndoRedoContent;
