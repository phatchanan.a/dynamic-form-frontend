// import { ReactKeycloakProvider } from "@react-keycloak/web";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { App } from "./App";
import "./index.css";
// import keycloak from './keycloak';
import reducers from "./reducers";
// import Protected from './screens/protected';
import * as serviceWorker from "./serviceWorker";

const store = createStore(reducers, applyMiddleware(thunk, logger));

// const keycloakInitOption = {
//   onLoad: "check-sso",
//   silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html'
// } as Keycloak.KeycloakInitOptions;

// const onEvent = (event, error) => {
//   switch (event) {
//     case 'onReady': {
//       if (!keycloak.authenticated) {
//         keycloak.init(keycloakInitOption).then((auth) => !auth && keycloak.login());
//       }
//       break;
//     }
//     case 'onAuthSuccess': {
//       localStorage.removeItem('curUser');
//       localStorage.removeItem('userTokens');
//       localStorage.removeItem('accessTokens');
//       break;
//     }
//     case 'onInitError': {
//       window.stop();

//       console.log('onInitError', error)

//       if (!keycloak.authenticated) {
//         keycloak.init(keycloakInitOption).then((auth) => !auth && keycloak.login());
//       }

//       // if (!keycloak.authenticated) {
//       //   keycloak.init({
//       //     onLoad: "login-required",
//       //     checkLoginIframe: false
//       //   } as Keycloak.KeycloakInitOptions);
//       // }
//       break;
//     }
//   }
// };

// const ReduxApp = () => (
//   <ReactKeycloakProvider
//     authClient={keycloak}
//     onEvent={onEvent}
//     onTokens={(tokens) => {
//       localStorage.setItem('userTokens', JSON.stringify(tokens));
//     }}
//     autoRefreshToken={true}
//     initOptions={keycloakInitOption}>
//     <Provider store={store}>
//       <Protected props={keycloak}>
//         <App keycloak={keycloak} />
//       </Protected>
//     </Provider>
//   </ReactKeycloakProvider>
// );

const ReduxApp = () => (

  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(
  // <React.StrictMode>
  <BrowserRouter>
    <ReduxApp />
  </BrowserRouter>
  // </React.StrictMode>
  , document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
