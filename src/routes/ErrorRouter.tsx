import React from "react";
import { Route } from "react-router-dom";

export const ErrorRouter = () => {
  return (
    <Route>
      <div>404</div>
    </Route>
  );
};
