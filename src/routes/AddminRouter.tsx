import React from "react";
import { Route } from "react-router-dom";

export const AddminRouter = () => {
  return (
    <Route
      path="/admin"
      render={({ match: { url } }) => (
        <>
          <Route exact path={`${url}/`}>
            <div>admin Page</div>
          </Route>
          <Route path={`${url}/home`}>
            <div>admin home Page</div>
          </Route>
          <Route path={`${url}/about`}>
            <div>admin about Page</div>
          </Route>
          <Route path={`${url}/upload`}>
            <div>Upload about Page</div>
          </Route>
          <Route path={`${url}/project`}>
            <div>Project about Page</div>
          </Route>
        </>
      )}
    />
  );
};
