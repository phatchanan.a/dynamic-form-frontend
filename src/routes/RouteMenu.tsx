import React from 'react';
import HomeIcon from '@material-ui/icons/Home';
import AssignmentIcon from '@material-ui/icons/Assignment';
import FolderSharedIcon from '@material-ui/icons/FolderShared';
import DescriptionIcon from '@material-ui/icons/Description';
import ApprovalIcon from '@material-ui/icons/LibraryAddCheck';
import SettingsIcon from '@material-ui/icons/Settings';

import { Home } from "../screens/home";
// import { ListForm } from "../screens/form/list";
// import { ProjectList } from "../screens/project";
// import { ReportProject } from "../screens/report/project";
import { Approval } from '../screens/approval';
import { Role } from '../screens/admin/projectRole';
import { UserProfile } from '../screens/admin/userProfile';

const Routes = [
	{
		path: '/',
		role: 'home',
		sidebarName: 'Home',
		component: () => <Home />,
		icon: <HomeIcon />,
		subRoute: []
	},
	// {
	// 	path: '/project',
	// 	role: 'project',
	// 	sidebarName: 'Project',
	// 	component: () => <ProjectList />,
	// 	icon: <FolderSharedIcon />,
	// 	subRoute: []
	// },
	{
		path: '/approval',
		role: 'approval',
		sidebarName: 'Approval',
		component: () => <Approval />,
		icon: <ApprovalIcon />,
		subRoute: []
	},
	// {
	// 	path: '/report/project',
	// 	role: 'report',
	// 	sidebarName: 'Report',
	// 	component: () => <ReportProject />,
	// 	icon: <DescriptionIcon />,
	// 	subRoute: []
	// },
	// {
	// 	path: '/form/list',
	// 	role: 'form',
	// 	sidebarName: 'Forms',
	// 	component: () => <ListForm />,
	// 	icon: <AssignmentIcon />,
	// 	subRoute: []
	// },
	{
		path: '',
		role: 'admin',
		sidebarName: 'Admin',
		component: () => { },
		icon: <SettingsIcon />,
		subRoute: [
			{
				path: '/admin/user',
				sidebarName: 'User Profile',
				component: () => <UserProfile />,
				icon: <SettingsIcon />
			},
			{
				path: '/admin/role',
				sidebarName: 'Project Role',
				component: () => <Role />,
				icon: <SettingsIcon />
			},
		]
	},
];

export default Routes;