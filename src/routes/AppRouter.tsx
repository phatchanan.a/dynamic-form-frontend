import jwt_decode from "jwt-decode";
import React, { useEffect, useState } from "react";
import { BrowserRouter, Redirect, Route, Switch, useLocation } from "react-router-dom";
import { ResultDlg } from "../components/MsgDlg";
import { Role } from "../screens/admin/projectRole";
import { UserProfile } from "../screens/admin/userProfile";
import { Approval } from "../screens/approval";
import { Approve } from "../screens/approval/approve";
import { Form } from "../screens/form";
// import ListForm from "../screens/form/list";
import Headers from '../screens/header';
import { Home } from "../screens/home";
import { Login } from "../screens/login/Login";
import { ResetPassword } from "../screens/login/ResetPassword";
// import { ProjectList } from "../screens/project";
// import { ProjectAssign } from "../screens/project/assign";
// import { UploadForm } from "../screens/project/package";
// import { PackageList } from "../screens/project/packageList";
// import { ReportProject } from "../screens/report/project";
// import { ReportProjectPackage } from "../screens/report/project/ProjectPackage";
import { httpService } from "../services";
import userManager from "../userManager";

export const loginSuccess = (res, setAccessTokens, setCurUser, userType = 'ADFS') => {
  let curUser: any = jwt_decode(res.item.access_token);
  // localStorage.clear();
  localStorage.setItem('accessTokens', res.item.access_token);
  localStorage.setItem('curUser', JSON.stringify({ ...curUser, LocalUserID: res.item.LocalUserID }));
  localStorage.setItem('userType', userType);

  // curUser.MenuList = ["home", "form", "project", "approval", "report", "admin"];
  // localStorage.setItem('curUser', JSON.stringify(curUser));

  setAccessTokens(res.item.access_token);
  setCurUser({ ...curUser, LocalUserID: res.item.LocalUserID });
}

export const AppRouter = (props: any) => {
  // const MenuList = props.curUser.MenuList;

  const [accessTokens, setAccessTokens] = useState(null);
  const [curUser, setCurUser] = React.useState(null);
  const [readyState, setReadyState] = React.useState(false);
  const params = useLocation<any>();

  useEffect(() => {
    // debugger
    let code = new URLSearchParams(params.search).get("code");
    let accessTokens = localStorage.getItem('accessTokens');
    let curUser = JSON.parse(localStorage.getItem("curUser"));

    if (accessTokens && curUser) {
      setAccessTokens(accessTokens);
      setCurUser(curUser);

      setReadyState(true);
    }

    if (['/login'].indexOf(params.pathname) === -1) {
      if (!accessTokens && !code) { // Redirec to ADFS Server.

        localStorage.clear();
        localStorage.setItem('redirectURL', window.location.pathname);

        const state_reg = new RegExp('[&]state=\\w*');
        const scope_reg = new RegExp('[&]scope=\\w*');
        const code_chellenge_reg = new RegExp('[&]code_challenge=\\w*');
        const code_chellenge_method_reg = new RegExp('[&]code_challenge_method=\\w*');

        userManager.createSigninRequest().then(res => {
          res.url = res.url.replace(state_reg, ''); //remove state_reg from url
          res.url = res.url.replace(scope_reg, ''); //remove scope from url
          res.url = res.url.replace(code_chellenge_reg, ''); //remove code_chellenge from url
          res.url = res.url.replace(code_chellenge_method_reg, ''); //remove code_chellenge_method from url
          window.location.href = res.url;
        }).catch(err => {
          console.log(err);
        });

        // userManager.signinRedirect();

      } else if (!accessTokens && code) {

        httpService.get(`auth/validate`, {
          params: {
            'code': code
          }
          //headers: { 'Authorization': `Bearer ${keycloak.token}` }
        }).then(res => {
          setReadyState(true);
          // if (res.item.access_token !== undefined) {
          //   loginSuccess(res, setAccessTokens, setCurUser);
          // }
          // else {
          //   ResultDlg(res);
          // }
        });

      }
    } else {
      setReadyState(true);
    }

  }, []);

  const Routes = [
    {
      role: 'home',
      component: [
        {
          path: "/",
          child: <Home />
        },
      ]
    },
    // {
    //   role: 'project',
    //   component: [
    //     {
    //       path: "/project",
    //       child: <ProjectList />
    //     },
    //     {
    //       path: "/project/:projectID",
    //       child: <PackageList />
    //     },
    //     {
    //       path: "/project/:projectID/package/:projDcpID",
    //       child: <UploadForm />
    //     },
    //     {
    //       path: "/project/:projectID/assign/:projectDisciplineID",
    //       child: <ProjectAssign />
    //     },
    //   ]
    // },
    {
      role: 'approval',
      component: [
        {
          path: "/approval",
          child: <Approval />
        },
        {
          path: "/approval/:ProjectID/:FormID",
          child: <Approve />
        },
      ]
    },
    // {
    //   role: 'report',
    //   component: [
    //     {
    //       path: "/report/project",
    //       child: <ReportProject />
    //     },
    //     {
    //       path: "/report/project/:projectID",
    //       child: <ReportProjectPackage />
    //     },
    //   ]
    // },
    {
      role: 'form',
      component: [
        {
          path: "/form/list",
          // child: <ListForm />
        },
        {
          path: "/form/create",
          child: <Form />
        },
        {
          path: "/form/:formID",
          child: <Form />
        },
      ]
    },
    {
      role: 'admin',
      component: [
        {
          path: "/admin/role",
          child: <Role />
        },
        {
          path: "/admin/user",
          child: <UserProfile />
        },
      ]
    },

  ];

  return (
    readyState &&
    <BrowserRouter>
      <Headers curUser={curUser} setCurUser={setCurUser} />
      <Switch>
        <Route exact path={"/auth"}
        // render={props => {
        //   let redirectURL = localStorage.getItem("redirectURL") || '/';
        //   return <Redirect to={`${redirectURL}`} />
        // }}
        >
          <Redirect to={localStorage.getItem("redirectURL") != "/auth" ? localStorage.getItem("redirectURL") : "/"} />
        </Route>

        <Route exact path={'/login'}>
          {
            //accessTokens && curUser && <Redirect to={localStorage.getItem("redirectURL") || "/"} />
            accessTokens && curUser && <Redirect to={"/"} />
          }
          <Login setAccessTokens={setAccessTokens} setCurUser={setCurUser} curUser={curUser} />
        </Route>

        <Route exact path={'/resetpassword'}>
          {accessTokens && curUser?.LocalUserID && <ResetPassword setAccessTokens={setAccessTokens} setCurUser={setCurUser} curUser={curUser} />}
        </Route>

        {accessTokens && curUser && Routes.filter(route => curUser.MenuList?.filter(mlist => mlist === route.role).length !== 0).map((route, i) => {
          return route.component.map((comp, i) =>
            <Route exact path={comp.path}>
              {comp.child}
            </Route>
          )
        })}

        <Route exact path="*">
          <Redirect to={(accessTokens && curUser) ? "/" : "/login"} />
        </Route>
      </Switch>
    </BrowserRouter>
  );

}
