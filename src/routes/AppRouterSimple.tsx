import jwt_decode from "jwt-decode";
import React, { useEffect, useState } from "react";
import { BrowserRouter, Redirect, Route, Switch, useLocation } from "react-router-dom";
import { ResultDlg } from "../components/MsgDlg";
// import { Role } from "../screens/admin/projectRole";
// import { UserProfile } from "../screens/admin/userProfile";
// import { Approval } from "../screens/approval";
// import { Approve } from "../screens/approval/approve";
import { Form } from "../screens/form";
// import ListForm from "../screens/form/list";
import Headers from '../screens/header';
import { Home } from "../screens/home";
// import { Login } from "../screens/login/Login";
// import { ResetPassword } from "../screens/login/ResetPassword";
// import { ProjectList } from "../screens/project";
// import { ProjectAssign } from "../screens/project/assign";
// import { UploadForm } from "../screens/project/package";
// import { PackageList } from "../screens/project/packageList";
// import { ReportProject } from "../screens/report/project";
// import { ReportProjectPackage } from "../screens/report/project/ProjectPackage";
import { httpService } from "../services";
import userManager from "../userManager";

export const loginSuccess = (res, setAccessTokens, setCurUser, userType = 'ADFS') => {
  let curUser: any = jwt_decode(res.item.access_token);
  // localStorage.clear();
  localStorage.setItem('accessTokens', res.item.access_token);
  localStorage.setItem('curUser', JSON.stringify({ ...curUser, LocalUserID: res.item.LocalUserID }));
  localStorage.setItem('userType', userType);

  // curUser.MenuList = ["home", "form", "project", "approval", "report", "admin"];
  // localStorage.setItem('curUser', JSON.stringify(curUser));

  setAccessTokens(res.item.access_token);
  setCurUser({ ...curUser, LocalUserID: res.item.LocalUserID });
}

export const AppRouterSimple = (props: any) => {
  // const MenuList = props.curUser.MenuList;

  const [accessTokens, setAccessTokens] = useState(null);
  const [curUser, setCurUser] = React.useState(null);
  const [readyState, setReadyState] = React.useState(false);
  const params = useLocation<any>();


  const Routes = [
    {
      role: 'home',
      component: [
        {
          path: "/",
          child: <Home />
        },
      ]
    },
    {
      role: 'project',
      component: [
        // {
        //   path: "/project",
        //   child: <ProjectList />
        // },
        // {
        //   path: "/project/:projectID",
        //   child: <PackageList />
        // },
        // {
        //   path: "/project/:projectID/package/:projDcpID",
        //   child: <UploadForm />
        // },
        // {
        //   path: "/project/:projectID/assign/:projectDisciplineID",
        //   child: <ProjectAssign />
        // },
      ]
    },
    {
      role: 'approval',
      component: [
        // {
        //   path: "/approval",
        //   child: <Approval />
        // },
        // {
        //   path: "/approval/:ProjectID/:FormID",
        //   child: <Approve />
        // },
      ]
    },
    {
      role: 'report',
      component: [
        // {
        //   path: "/report/project",
        //   child: <ReportProject />
        // },
        // {
        //   path: "/report/project/:projectID",
        //   child: <ReportProjectPackage />
        // },
      ]
    },
    {
      role: 'form',
      component: [
        // {
        //   path: "/form/list",
        //   child: <ListForm />
        // },
        // {
        //   path: "/form/create",
        //   child: <Form />
        // },
        {
          path: "/form/:formKey",
          child: <Form />
        },
      ]
    },
    {
      role: 'admin',
      component: [
        // {
        //   path: "/admin/role",
        //   child: <Role />
        // },
        // {
        //   path: "/admin/user",
        //   child: <UserProfile />
        // },
      ]
    },

  ];

  return (
    <BrowserRouter>
      <Headers curUser={curUser} setCurUser={setCurUser} />
      <Switch>

        {Routes.map((route, i) => {
          return route.component.map((comp, i) =>
            <Route exact path={comp.path}>
              {comp.child}
            </Route>
          )
        })}

        <Route exact path="*">
          <Redirect to={(accessTokens && curUser) ? "/" : "/login"} />
        </Route>
      </Switch>
    </BrowserRouter>
  );

}
