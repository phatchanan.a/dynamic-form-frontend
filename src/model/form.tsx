import { ResponseViewModel, BaseViewModel, PaginationViewModel } from ".";
export * from ".";

export interface PropsExpand {
	title: string;
	formDetails: FormDetails[];
}

export interface Items {
	id?: number;
	title: string;
	key: string;
	icon: JSX.Element;
	props: Record<string, string | boolean>;
}

export interface FormViewModel {
	variables?: {},
	formKey?: string,
	formFields?: FormFieldsViewModel[]
}


export interface FormFieldsViewModel {
	businessKey?: boolean,
	id?: string,
	label?: string,
	type?: {
		name?: string
	},
	defaultValue?: any,
	value?: {
		value?: any,
		type?: {
			name?: string,
			javaType?: string,
			primitiveValueType?: boolean,
			parent?: string,
			abstract?: boolean
		},
		transient?: boolean
	},
	validationConstraints?: [],
	properties?: {},
	typeName?: string
}



export interface FormDetails {
	id?: string,
	icon?: JSX.Element,
	error: boolean,
	FormDetailID?: number,
	FieldTypeID: number,
	FieldKey: string,
	FieldType: string,
	SequenceNo: number,
	Properties: { [key: string]: string | any } | { [key: string]: any[] }
}

export interface SubProps {
	id: string;
	index: number;
	fieldProps: {
		[key: string]: {
			required?: boolean,
			value?: any,
			error?: boolean,
			hidden?: boolean,
			readOnly?: boolean,
			rows?: number,
			limit?: number,
		}
	}
}