import { ResponseViewModel, BaseViewModel, PaginationViewModel } from ".";
export * from ".";

export interface FormReportViewModel extends BaseViewModel {
  ItpID: number,
  FormKey: string,
  SequenceNo: number,
  InspectLevelID: number,
  InspectLevel: number,
  FormName: string,
  FormID:number,
}

export interface InspReportViewModel extends BaseViewModel {
  InspectionID: number,
  ItpID: number,
  FormID: number,
  FormKey: string,
  FormName: string,
  InspectLevel: string,
  InspectDate:string,
  LineNo: string,
  DrawingNo: string,
  JointNo: string,
  JointSize: string,
  JointType: string,
  JointSf: string,
}