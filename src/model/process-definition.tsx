import { ResponseViewModel, BaseViewModel, PaginationViewModel } from ".";
export * from ".";

export interface ProcessDefinitionModel {
  id?: string,
  key?: string,
  category?: string,
  description?: string,
  name?: string,
  version?: number,
  resource?: string,
  deploymentId?: string,
  diagram?: any,
  suspended?: boolean,
  tenantId?: string,
  versionTag?: string,
  historyTimeToLive?: any,
  startableInTasklist?: boolean
}

