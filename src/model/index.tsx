import { ColumnOptional } from "../components/DataTable";

export interface ResponseViewModel<T extends BaseViewModel = any> {
	data?: T,
	result?: boolean,

	item?: any,
	items?: any[],
	error?: string,
	message?: string,
	statusCode?: number,
	meta?: PaginationViewModel,
}

export interface SearchViewModel<T = any> {
	page?: number,
	limit?: number,
	conditions?: string,
	orderBy?: keyof T,
	order?: { [key: string]: keyof { 'asc', 'desc' } },
	direction?: keyof { 'asc', 'desc' },
}

export interface BaseViewModel {
	CurUserName?: string,
	CreateBy?: string;
	CreateDate?: any;
	UpdateBy?: string;
	UpdateDate?: any;
}

export interface PaginationViewModel {
	currentPage?: number,
	itemCount?: number,
	itemsPerPage?: number,
	totalItems?: number,
	totalPages?: number,
}

export interface UserViewModel {
	UserName?: string,
	UserID?: number,
	Project?: [
		{
			ProjectId: number,
			ApprovalLevleID: number,
		}
	],
	iat?: any,
	exp?: any,
}