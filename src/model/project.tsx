import { ResponseViewModel, BaseViewModel, PaginationViewModel } from ".";
export * from ".";

export interface ProjectListViewModel extends BaseViewModel {
  ProjectID?: number;
  ProjectNo?: string;
  ProjectName?: string;
  PlantID?: number;
  PlantName?: string,
  Area?: string;
  Status?: string;
  Favorite?: string;
  DisciplineList?: {
    ProjectDisciplinID: number,
    DisciplineID: number,
    DisciplineName: string,
  }[]
}

export interface PlantModel {
  PlantID: number,
  PlantName: string,
  Description: string,
  ActiveFlag: string,
  CreateBy: string,
  CreateDate: string,
  UpdateBy: string,
  UpdateDate: string
}

export interface ApprViewModel extends BaseViewModel {
  ApprovalLevelID?: string,
  ApprovalLevel?: string,
  Description?: string,
  Status?: string,
  Check?: string,
}