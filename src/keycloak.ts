import Keycloak from "keycloak-js";

// function initialKeyCloak(): Keycloak.KeycloakInstance {
//   const keycloak: Keycloak.KeycloakInstance = Keycloak({
//     url: 'https://keycloak.scg.extenditresource.com/auth',
//     realm: 'master',
//     clientId: 'repco',
//   });

//   return keycloak;
// }

export function updateToken(keycloak: Keycloak.KeycloakInstance): void {
  keycloak
    .updateToken(20)
    .then((refreshed: boolean) => {
      console.log("Keycloak Token is refreshed", refreshed);
    })
    .catch((err) => {
      console.log("Keycloak Token refresh error, ", err);
    });
}

const keycloak = Keycloak({
  url: "https://keycloak.scg.extenditresource.com/auth",
  realm: "master",
  clientId: "repco",
});

export default keycloak;
