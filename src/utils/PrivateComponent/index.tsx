import React from 'react';
import { connect } from 'react-redux';
import { UserState } from '../../reducers/user.reducer';

interface PrivateComponentProps {
  user: UserState,
  roles: string[],
  mainComponent: JSX.Element,
  notPermitComponent?: JSX.Element
}

interface PrivateComponentState {

}


class PrivateComponent extends React.Component<PrivateComponentProps, PrivateComponentState> {

  hasAuthorize(): boolean {
    if (this.props.roles.length === 0) {
      return true;
    }

    if (this.props.user.keycloak && this.props.roles) {
      return this.props.roles.some((r: string) => {
        const realm = this.props.user.keycloak.hasRealmRole(r);
        const resource = this.props.user.keycloak.hasResourceRole(r);
        return realm || resource;
      });
    }

    return false;
  }

  render() {
    return (
      <>
        {(() => {
          if (this.hasAuthorize()) {
            if (typeof (this.props.mainComponent) !== 'undefined') {
              return this.props.mainComponent;
            } else if (typeof (this.props.children) !== 'undefined') {
              return this.props.children;
            } else {
              return null;
            }
          } else {
            if (typeof (this.props.notPermitComponent) !== 'undefined') {
              return this.props.notPermitComponent;
            } else {
              return null;
            }
          }
        })()}
      </>
    );
  }

}

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps)(PrivateComponent);