import React from 'react';
import { connect } from 'react-redux';
import { Route, RouteComponentProps, withRouter } from 'react-router-dom';
import { UserState } from '../../reducers/user.reducer';
import './index.css';

interface PrivateRouteProps extends RouteComponentProps<any> {
  user: UserState,
  roles: string[]
  path: string,
  props?: React.ComponentProps<any>
}

interface PrivateRouteState {

}

class PrivateRoute extends React.Component<PrivateRouteProps, PrivateRouteState> {

  hasAuthorize(): boolean {
    if (this.props.roles.length === 0) {
      return true;
    }

    if (this.props.user.keycloak && this.props.roles) {
      return this.props.roles.some((r: string) => {
        const realm = this.props.user.keycloak.hasRealmRole(r);
        const resource = this.props.user.keycloak.hasResourceRole(r);
        return realm || resource;
      });
    }

    return false;
  }

  render() {
    return (
      <Route
        render={props => this.hasAuthorize() ? (
          <Route path={this.props.path} children={this.props.children} {...this.props.props} />
        ) : (
            <div className="center">
              <h3>You not permit to page</h3>
            </div>
          )
        } />
    );
  }

}

const mapStateToProps = (state) => ({
  user: state.user
});

export default withRouter(connect(mapStateToProps)(PrivateRoute));