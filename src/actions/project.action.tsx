import {
  ACTION_ADD_PROJECT,
} from "../constants/constants";

export const setStateAddProject = (payload: any) => ({
  type: ACTION_ADD_PROJECT,
  payload,
});

export const addProject = (payload: any) => {
  return (dispatch: any) => {
    dispatch(setStateAddProject(payload));
  };
};
