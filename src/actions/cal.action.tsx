import { ACTION_ADD, ACTION_REM, ACTION_CLR } from "../constants/constants";
export const setStateToADD = () => ({
  type: ACTION_ADD,
});

export const setStateToREM = () => ({
  type: ACTION_REM,
});

export const setStateToCLR = (payload: any) => ({
  type: ACTION_CLR,
  payload,
});

export const add = () => {
  return (dispatch: any) => {
    dispatch(setStateToADD());
  };
};

export const remove = () => {
  return (dispatch: any) => {
    dispatch(setStateToREM());
  };
};

export const clear = (payload: any) => {
  return (dispatch: any) => {
    dispatch(setStateToCLR(payload));
  };
};

export const divi = (a: any, b: any) => {
  return a / b
}
