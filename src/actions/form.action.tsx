import { ACTION_ADD_FORM} from "../constants/constants";
export const setStateAddForm = (payload: any) => ({
  type: ACTION_ADD_FORM,
  payload,
});

export const addForm = (payload: any) => {
  return (dispatch: any) => {
      dispatch(setStateAddForm(payload));
  };
};


