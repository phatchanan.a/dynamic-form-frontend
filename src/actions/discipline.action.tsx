import {
  ACTION_CLICK_DISCIPLINE,
} from "../constants/constants";


export const setStateClickDiscipline = (payload: any) => ({
  type: ACTION_CLICK_DISCIPLINE,
  payload,
});

export const addClickDiscipline = (payload: any) => {
  return (dispatch: any) => {
    dispatch(setStateClickDiscipline(payload));
  };
};