import * as H from 'history';
import { UserAction } from '../reducers/user.reducer';

export const ACTION_USER_LOGIN = 'ACTION_USER_LOGIN';
export const ACTION_USER_LOGOUT = 'ACTION_USER_LOGOUT';
export const ACTION_USER_UPDATE_REDIRECT_AUTH = 'ACTION_USER_UPDATE_REDIRECT_AUTH';
export const ACTION_USER_GET_REDIRECT_AUTH = 'ACTION_USER_GET_REDIRECT_AUTH';

export function userLogin(history: H.History) {
  return {
    type: ACTION_USER_LOGIN,
    history
  } as UserAction;
}

export function userLogout(history: H.History) {
  return {
    type: ACTION_USER_LOGOUT,
    history
  } as UserAction;
}

export interface UserRedirectPayload {
  history: H.History,
  redirectTo: H.Location
}

export function userRedirectUpdate(payload: UserRedirectPayload) {
  return {
    type: ACTION_USER_UPDATE_REDIRECT_AUTH,
    history: payload.history,
    redirectTo: payload.redirectTo
  } as UserAction;
}
