import {
    ACTION_ADD_FIELDS,
    ACTION_ADD_ALL_FIELDS,
    ACTION_REMOVE_FIELDS,
    ACTION_CHANGE_FIELDS_ORDER,
    ACTION_CHANGE_FIELDS_PROPERTIES,
} from "../constants/constants";

export const setStateAddFields = (payload: any) => ({
    type: ACTION_ADD_FIELDS,
    payload,
});

export const setStateAddAllFields = (payload: any) => ({
    type: ACTION_ADD_ALL_FIELDS,
    payload,
});

export const setStateRemoveFields = (payload: number) => ({
    type: ACTION_REMOVE_FIELDS,
    payload,
});

export const setStateChangeFieldsProperties = (payload: any) => ({
    type: ACTION_CHANGE_FIELDS_PROPERTIES,
    payload,
});

export const setStateChangeFieldsOrder = (payload: any) => ({
    type: ACTION_CHANGE_FIELDS_ORDER,
    payload,
});

export const addFields = (payload: any) => {
    return (dispatch: any) => {
        dispatch(setStateAddFields(payload));
    };
};

export const addAllFields = (payload: any) => {
    return (dispatch: any) => {
        dispatch(setStateAddAllFields(payload));
    };
};

export const removeFields = (payload: number) => {
    return (dispatch: any) => {
        dispatch(setStateRemoveFields(payload));
    };
};

export const changeFields = (payload: any) => {
    return (dispatch: any) => {
        dispatch(setStateChangeFieldsProperties(payload));
    };
};
