import {
  ACTION_PREVIEW_PROPERTIES,
} from "../constants/constants";

export const setStateAddProperties = (payload: number) => ({
  type: ACTION_PREVIEW_PROPERTIES,
  payload,
});

export const addProperties = (payload: number) => {
  return (dispatch: any) => {
    dispatch(setStateAddProperties(payload));
  };
};