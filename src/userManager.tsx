import { UserManager, UserManagerSettings } from 'oidc-client';
import { createUserManager } from 'redux-oidc';

const userManagerConfig: UserManagerSettings | any = {
  client_id: 'QAQC-SYSTEM',
  redirect_uri: `${window.location.protocol}//${window.location.hostname}${window.location.port ? `:${window.location.port}` : ''}/auth`,
  // post_logout_redirect_uri: `${window.location.protocol}//${window.location.hostname}${window.location.port ? `:${window.location.port}` : ''}/auth`,
  response_type: 'code',
  // response_mode: 'form_post',
  resource: 'QAQC-SYSTEM',
  // scope: '',
  authority: 'http://accessdev.scg.co.th',
  metadata: {
    authorization_endpoint: 'https://accessdev.scg.co.th/adfs/oauth2/authorize',
    end_session_endpoint: 'https://accessdev.scg.co.th/adfs/ls/?wa=wsignout1.0'
  }
};

const userManager: UserManager = createUserManager(userManagerConfig);

export default userManager;
