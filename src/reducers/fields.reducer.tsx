import undoable from "redux-undo";
import {
  ACTION_ADD_FIELDS,
  ACTION_ADD_ALL_FIELDS,
  ACTION_REMOVE_FIELDS,
  ACTION_CHANGE_FIELDS_ORDER,
  ACTION_CHANGE_FIELDS_PROPERTIES,
} from "../constants/constants";
import { InitialStateFields, PropsAction } from "./reducers.interface";
import { v4 as uuidv4 } from "uuid";

const initialState: InitialStateFields = {
  content_data: [],
};

const content = (
  state = initialState,
  { type, payload }: PropsAction
): InitialStateFields => {
  switch (type) {
    case ACTION_ADD_FIELDS:
      return {
        content_data: [...state.content_data, {
          ...payload, id: uuidv4(),
          // error: (payload.FieldKey !== "page_break" && payload.FieldKey !== "punch_remark" ? true : false)
          error: Object.keys(payload.Properties).map(e => payload.Properties[e].error).filter(e => e).length > 0 ? true : false
        }],
      };
    case ACTION_ADD_ALL_FIELDS:
      return {
        content_data: [...payload],
      };
    case ACTION_REMOVE_FIELDS:
      let newState = state.content_data.slice();
      if (typeof payload === 'object' && Array.isArray(payload)) {
        payload.map(del => newState.splice(del, 1));
      }
      else {
        newState.splice(payload, 1);
      }
      return { ...state, content_data: newState };
    case ACTION_CHANGE_FIELDS_PROPERTIES:
      return {
        ...state,
        content_data: state.content_data.map((item: any, index: number) => {
          if (index !== payload.index) {
            return item;
          }

          let Properties = {
            ...item.Properties,
            [payload.PropertiesKey]: { ...item.Properties[payload.PropertiesKey], ...payload.PropertiesValue },
          }

          return {
            ...item,
            // error: isNullOrUndefined(payload.PropertiesValue.error)? item.error: payload.PropertiesValue.error,
            error: Object.entries(Properties).filter(([key, value]) => { return (value as any).error }).length > 0 ? true : false,
            Properties: Properties,
          };
        }),
      };
    case ACTION_CHANGE_FIELDS_ORDER:
      return {
        ...state,
        content_data: state.content_data.map((item: any, index: number) => {
          return {
            ...item,
            SequenceNo: payload.indexOf(item.FieldKey + "_" + item.id),
          };
        }),
      };

    //=================================== field list ===================================

    case 'ACTION_ADD_FIELDS_LIST':
      return {
        ...state,
        content_data: state.content_data.map((item: any, index: number) => {
          if (index !== payload.index) {
            return item;
          }

          let Properties = {
            ...item.Properties,
            [payload.PropertiesKey]: {
              ...item.Properties[payload.PropertiesKey],
              value: [...item.Properties[payload.PropertiesKey].value, payload.PropertiesValue.value],
              error: payload.PropertiesValue.error,
            }
          }

          return {
            ...item,
            error: Object.entries(Properties).filter(([key, value]) => { return (value as any).error }).length > 0 ? true : false,
            Properties: Properties,
          };
        }),
      };

    case 'ACTION_CHANGE_FIELDS_LIST':
      return {
        ...state,
        content_data: state.content_data.map((item: any, index: number) => {
          if (index !== payload.index) {
            return item;
          }

          let Properties = {
            ...item.Properties,
            [payload.PropertiesKey]: {
              ...item.Properties[payload.PropertiesKey],
              value: item.Properties[payload.PropertiesKey].value.map((item: any, i: number) => {
                if (i === payload.PropertiesValue.index) {
                  return { ...item, ListValue: payload.PropertiesValue.value };
                } else return item;
              }),
              error: payload.PropertiesValue.error,
            }
          }

          return {
            ...item,
            error: Object.entries(Properties).filter(([key, value]) => { return (value as any).error }).length > 0 ? true : false,
            Properties: Properties,
          };
        }),
      };

    case 'ACTION_CHANGE_ORDER_FIELDS_LIST':
      return {
        ...state,
        content_data: state.content_data.map((item: any, index: number) => {
          if (index !== payload.index) {
            return item;
          }

          let Properties = {
            ...item.Properties,
            [payload.PropertiesKey]: {
              ...item.Properties[payload.PropertiesKey],
              value: item.Properties[payload.PropertiesKey].value.map((item: any, i: number) => {
                return { ...item, SequenceNo: payload.PropertiesValue.order.indexOf(item.id) };
              }),
              error: item.error,
            }
          }

          return {
            ...item,
            error: Object.entries(Properties).filter(([key, value]) => { return (value as any).error }).length > 0 ? true : false,
            Properties: Properties,
          };
        }),
      };

    case 'ACTION_REMOVE_FIELDS_LIST':
      return {
        ...state,
        content_data: state.content_data.map((item: any, index: number) => {
          if (index !== payload.index) {
            return item;
          }

          let newValue = item.Properties[payload.PropertiesKey].value.slice();
          newValue.splice(payload.PropertiesValue.index, 1);

          let Properties = {
            ...item.Properties,
            [payload.PropertiesKey]: {
              ...item.Properties[payload.PropertiesKey],
              value: newValue,
              error: newValue.length === 0,
            }
          }

          return {
            ...item,
            error: Object.entries(Properties).filter(([key, value]) => { return (value as any).error }).length > 0 ? true : false,
            Properties: Properties,
          };
        }),
      };

    default:
      return state;
  }
};

const undoableContents = undoable(content as any);
export default undoableContents;
