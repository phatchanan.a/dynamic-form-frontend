import { ACTION_ADD_FORM, } from "../constants/constants";
import { PropsForm } from "./reducers.interface";
import { FormViewModel } from "../model/form";

const formState: FormViewModel = {
  // FormName: '',
  // Description: '',
  // DisciplineID: 0,
  // InspLevelID: 0,
};

export default (state = formState, { type, payload }: any) => {
  switch (type) {
    case ACTION_ADD_FORM:
      return {
        ...state,
        FormName: payload.FormName,
        Description: payload.Description,
        DisciplineID: payload.DisciplineID,
        InspLevelID: payload.InspLevelID,
      } as PropsForm;
    default:
      return state;
  }
};
