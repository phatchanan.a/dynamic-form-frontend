import { combineReducers } from "redux";
import disciplineReducer from "./discipline.reducer";
import fieldsReducer from "./fields.reducer";
import formReducer from "./form.reducer";
import projectReducer from "./project.reducer";
import propertiesReducer from "./properties.reducer";
export default combineReducers({
    // user: userReducer,
    fieldsReducer,
    formReducer,
    propertiesReducer,
    projectReducer,
    disciplineReducer
});
