import { ACTION_ADD_PROJECT, } from "../constants/constants";

const formState = {
  ProjectId:'',
  ProjectNo: '',
  ProjectName: '',
};

export default (state = formState, { type, payload }: any) => {
  switch (type) {
    case ACTION_ADD_PROJECT:
      return {
        ...state,
        ProjectId:payload.ProjectId,
        ProjectNo: payload.ProjectNo,
        ProjectName: payload.ProjectName,
      };
    default:
      return state;
  }
};
