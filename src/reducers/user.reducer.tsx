import * as H from 'history';
import { ACTION_USER_LOGIN, ACTION_USER_LOGOUT, ACTION_USER_UPDATE_REDIRECT_AUTH } from '../actions/user.action';
import keycloak from '../keycloak';

/**
 * declare type
 */

export interface UserAction {
  type: string,
  history: H.History,
  redirectTo?: H.Location
}

export interface UserState {
  keycloak?: Keycloak.KeycloakInstance,
  redirectOnAuth?: H.Location,
  authenticated: boolean
}

/**
 * function
 */

const login = (state: UserState, action: UserAction) => {
  console.log('keycloak login => start');
  return state.keycloak.init({
    onLoad: "login-required",
    checkLoginIframe: false,
    silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html'
  }).then((auth) => {
    state.authenticated = auth;

  }).catch(() => {
    state.authenticated = false;
  }).finally(() => {
    return state;
  });



  // console.log('keycloak login => step 1', auth);
  // if (!auth) {
  //   await state.keycloak.login();

  //   return state;
  // } else {
  //   state.authenticated = auth;
  //   setTimeout(() => {
  //     console.log('keycloak login => step 3');
  //     if (state.redirectOnAuth) {
  //       action.history.push(state.redirectOnAuth.pathname, state.redirectOnAuth.state);
  //     } else {
  //       action.history.push('/');
  //     }
  //   }, 250);

  //   return state;
  // }
};

const logout = (state: UserState, action: UserAction) => {
  state.keycloak.logout().then(() => {
    state.keycloak.clearToken();
    state.authenticated = false;
    action.history.push('/protected');
  });
};

const updateRedirectOnAuth = (state: UserState, action: UserAction) => {
  state.redirectOnAuth = action.redirectTo;
  return state;
};


export default (state: UserState = { authenticated: false }, action: UserAction) => {
  if (typeof (state.keycloak) === 'undefined') {
    state.keycloak = keycloak;
  }

  switch (action?.type) {
    case ACTION_USER_LOGIN: return login(state, action);
    case ACTION_USER_LOGOUT: return logout(state, action);
    case ACTION_USER_UPDATE_REDIRECT_AUTH: return updateRedirectOnAuth(state, action);
    default: return state;
  }
}