import {
  ACTION_PREVIEW_PROPERTIES,
} from "../constants/constants";
import { PropsAction } from "./reducers.interface";
import { Items } from "../model/form";

interface PropertiesInitialState {
  index: number;
}
const initialState = {
  index: 0,
};

export default (
  state = initialState,
  { type, payload }: PropsAction
): PropertiesInitialState => {
  switch (type) {
    case ACTION_PREVIEW_PROPERTIES:
      return { ...state, index: payload };
    default:
      return { ...state };
  }
};
