import { FormDetails } from "../model/form";

export interface IinitialState {
  count: number;
}
export interface InitialStateFields {
  content_data: FormDetails[];
}
export interface PropsAction {
  type: string;
  payload: any;
}
export interface PropsForm {
  FormName: string;
  Description: string;
  DisciplineID: number;
  InspLevelID: number;
}
