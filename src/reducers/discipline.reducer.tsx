import { ACTION_CLICK_DISCIPLINE, } from "../constants/constants";

const disState = {
  DisciplineId:'',
  DisciplineName: '',
  Description: '',
};

export default (state = disState, { type, payload }: any) => {
  switch (type) {
    case ACTION_CLICK_DISCIPLINE:
      return {
        ...state,
        DisciplineId:payload.DisciplineId,
        DisciplineName: payload.DisciplineName,
        Description: payload.Description,
      };
    default:
      return state;
  }
};
