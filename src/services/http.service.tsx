import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { BaseViewModel, ResponseViewModel, UserViewModel } from "../model";
import { isNullOrUndefined } from "util";
import LoadingScreen from "../components/LoadingScreen";
import { ResultDlg } from "../components/MsgDlg";
import userManager from "../userManager";

const DELAY = 800;

axios.interceptors.request.use(async (config) => {

  let accessTokens = localStorage.getItem('accessTokens');

  if (accessTokens) {
    config.headers.Authorization = `Bearer ${accessTokens}`;
  }

  let curUser: UserViewModel = JSON.parse(localStorage.getItem('curUser'));

  var userData: BaseViewModel = {
    CurUserName: curUser?.UserName,
    CreateBy: curUser?.UserName,
    UpdateBy: curUser?.UserName,
  }


  if (config.data) {
    config.data = Object.assign(config.data, userData);
  }

  if (config.method === "delete") {
    if (config.url.indexOf("formheader/") === 0) {
      config.params = Object.assign(config.params || {}, userData);
    }
  }

  config.url = `${process.env.REACT_APP_API_URL}/${config.url}`;
  LoadingScreen();
  return config;

}, (error => {
  console.log(error);
  return sleep(DELAY).then(() => {
    LoadingScreen(false);
    return Promise.reject(error);
  });
}));

axios.interceptors.response.use(async (response) => {

  // if (response && typeof (response) === "object")
  //   if (response?.status === 200 || response?.status === 201) response.data.result = true;
  //   else response.data.result = false;

  let result: ResponseViewModel = {};

  if (response && typeof (response) === "object") {
    result.message = response.statusText;
    result.statusCode = response.status;
    result.data = response.data;
    result.item = response.data;
    result.items = response.data;
    if (response?.status === 200 || response?.status === 201) result.result = true;
    else result.result = false;
  }

  console.log(response);
  return sleep(DELAY).then(() => {
    LoadingScreen(false);
    return result as AxiosResponse;
  });

}, (error => {
  console.log(error);

  if (!error.response) error = { ...error, response: { data: { result: false, message: error.message } } };
  else error.response.data.result = false;

  return sleep(DELAY).then(() => {
    ResultDlg(error.response.data, { message: error.response.data.message ?? error.message });
    LoadingScreen(false);
    return error.response
  });

}));

// export const httpService = axios;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export class httpService {

  static post = async<T extends BaseViewModel = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<ResponseViewModel<T>> => {
    let result: ResponseViewModel<T> = {};
    await axios.post<ResponseViewModel<T>>(url, data, config).then(({ data }) => {
      result = data;
      httpService.sessionChecker(result);
    }).catch((error) => {
      console.log(error);
    });
    return result;
  };

  static put = async<T extends BaseViewModel = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<ResponseViewModel<T>> => {
    let result: ResponseViewModel<T> = {};
    await axios.put<ResponseViewModel<T>>(url, data, config).then(({ data }) => {
      result = data;
      httpService.sessionChecker(result);
    }).catch((error) => {
      console.log(error);
    });
    return result;
  };

  static patch = async<T extends BaseViewModel = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<ResponseViewModel<T>> => {
    let result: ResponseViewModel<T> = {};
    await axios.patch<ResponseViewModel<T>>(url, data, config).then(({ data }) => {
      result = data;
      httpService.sessionChecker(result);
    }).catch((error) => {
      console.log(error);
    });
    return result;
  };

  static delete = async<T extends any>(url: string, config?: AxiosRequestConfig): Promise<ResponseViewModel<T>> => {
    let result: ResponseViewModel<T> = {};
    await axios.delete<ResponseViewModel<T>>(url, config).then(({ data }) => {
      result = data;
      httpService.sessionChecker(result);
    }).catch((error) => {
      console.log(error);
    });
    return result;
  };

  static get = async<T extends any>(url: string, config?: AxiosRequestConfig): Promise<ResponseViewModel<T>> => {
    let result: ResponseViewModel<T> = {};
    await axios.get<T>(url, config).then((response) => {
      // if (response && typeof (response) === "object") {
      //   result.message = response.statusText;
      //   result.statusCode = response.status;
      //   result.data = response.data;
      //   if (response?.status === 200 || response?.status === 201) result.result = true;
      //   else result.result = false;
      // }
      // debugger
      result = response;
      httpService.sessionChecker(result);
    }).catch((error) => {
      console.log(error);
    });
    return result;
  };

  static getImage = async (url: string, config?: AxiosRequestConfig) => {
    let result: Blob;
    await axios.get(url, Object.assign(config, { responseType: 'blob' })).then(({ data }: any) => {
      result = data;
    }).catch((error) => {
      console.log(error);
    });
    return result;
  };

  static sessionChecker = (result: ResponseViewModel) => {
    if (!result.result) {
      if (result.statusCode === 401) ResultDlg(result, {
        onClose: () => {
          // httpService.redirectToLogin();

          let userType = localStorage.getItem('userType');
          localStorage.clear();

          // ADFS will redirect to adfs server and get new token
          if (userType === "ADFS") {
            window.location.href = "/";
          }

          // LOCAL will clear local token and user login again
          else if (userType === "local") {
            window.location.href = "/login";
          }

        },
        message: 'Session expired, please login again.'
      });
      else ResultDlg(result);

    }
  }

  static redirectToLogin = () => {
    if (document.getElementById('logout')) document.getElementById('logout').click();
    else {
      let userType = localStorage.getItem('userType');
      localStorage.clear();

      // debugger
      // if (userType === "ADFS") userManager.signoutRedirect();
      if (userType === "ADFS") {
        userManager.signoutRedirectCallback();
        // userManager.signinRedirect();

        // window.location.href = "/";
      }
      else if (userType === "local") {
        window.location.href = "/login";
      }

    }
  }

}


// export const endPoint = {
//   url: "https://jsonplaceholder.typicode.com/todos/1",
// };

// export async function get(url: string): Promise<any> {
//   const res = await axios.get(url);
//   return res;
// }

// export async function post(url: string, req: any): Promise<any> {
//   const res = await axios.post(url, req);
//   return res;
// }

// export async function put(url: string, req: any): Promise<any> {
//   const res = await axios.put(url, req);
//   return res;
// }