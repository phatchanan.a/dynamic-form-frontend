import { ThemeProvider } from "@material-ui/core/styles";
import React from "react";
// import { AppRouter } from "./routes/AppRouter";
import { AppRouterSimple } from "./routes/AppRouterSimple";
import { theme } from "./Theme/Theme";

export const App = (props: any) => {

  return (
    <ThemeProvider theme={theme}>
      <AppRouterSimple />
    </ThemeProvider>
  );

};
