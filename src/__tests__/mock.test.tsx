import React from "react";
import { render, fireEvent, waitForElement, screen } from "@testing-library/react"
import { ListForm } from "../screens/form/list"
import { Form } from "../screens/form"
import { Login } from "../screens/login/Login"
import * as Cal from "../actions/cal.action"

import { InitialStateFields, PropsAction } from "../reducers/reducers.interface"
import * as Field from "../reducers/fields.reducer"
import undoable from "redux-undo";
import * as FormReduce from "../reducers/form.reducer"
import * as Propperties from "../reducers/properties.reducer"
import { Properties } from "../screens/form/properties";
import FolderSharedIcon from '@material-ui/icons/FolderShared';
import * as FieldAction from "../actions/fields.action";
import * as FormAction from "../actions/form.action";
import * as PropertiesAction from "../actions/properties.action";
// import { shallow, configure } from 'enzyme';
// import Adapter from 'enzyme-adapter-react-16'
// configure({ adapter: new Adapter() })
const initialState: InitialStateFields = {
  content_data: [{
    id: "Paragraph",
    icon: <FolderSharedIcon />,
    error: true,
    FormDetailID: 1,
    FieldTypeID: 2,
    FieldKey: "string",
    FieldType: "string",
    SequenceNo: 3,
    Properties: {}
  },
    // {
    //   FieldKey: "Paragraph",
    //   FieldType: "Paragraph",
    //   FieldTypeID: 2,
    //   FormDetailID: 16,
    //   Properties: {
    //     Default_Value: "",
    //     Disabled_Field: "",
    //     Field_Label: "",
    //     Required_Field: "",
    //   },
    //   SequenceNo: 1,
    //   icon: undefined,
    //   id: "2"
    // }
  ],
};

describe("<Cal  /> ", () => {
  test(" divi function ", () => {
    expect(Cal.divi(1, 2)).toBe(0.5);
  })
})

describe(" Test Field Reducer ", () => {
  test(" Field.Reducer ====> ", () => {
    //expect(Field.default(undoable(initialState,{})).toHaveProperty(initialState)
  })
})

describe(" Form Reducer ", () => {
  const formState = {
    FormName: "New Form",
    Description: "New Description",
    DisciplineID: 0,
    InspLevelID: 0,
  };
  test("Test Form Reducer Get ", () => {
    expect(FormReduce.default(formState, { type: "ACTION_ADD_FORM", payload: formState })).toMatchObject(formState)
  });
});

describe(" Propperties Reducer ", () => {
  interface PropertiesInitialState {
    index: number;
  }
  const propperties = {
    index: 0,
  };
  test("Test Propperties ACTION_PREVIEW_PROPERTIES ", () => {
    expect(Propperties.default(propperties, { type: "ACTION_PREVIEW_PROPERTIES", payload: 0 })).toMatchObject(propperties)
  });
});


describe(" Test Jest In FORM ", () => {
  test("Form Function ===>  ", () => {
    // const wrapper = shallow(<ListForm />)
    // expect(wrapper.state('name')).toBe(0)
  });
});

describe(" Field Action ", () => {
  test(" setStateRemoveFields ", () => {
    expect(FieldAction.setStateRemoveFields(1).payload).toBe(1)
    expect(FieldAction.setStateRemoveFields(1).type).toBe("ACTION_REMOVE_FIELDS")
  });

  test(" setStateAddFields ", () => {
    expect(FieldAction.setStateAddFields({ payload: 1 }).payload.payload).toBe(1)
    expect(FieldAction.setStateAddFields({ payload: 1 }).type).toBe("ACTION_ADD_FIELDS")
  });

  test(" setStateAddAllFields ", () => {
    expect(FieldAction.setStateAddAllFields({ test: 1 }).payload.test).toBe(1)
    expect(FieldAction.setStateAddAllFields({ test: 1 }).type).toBe("ACTION_ADD_ALL_FIELDS")
  });

  test(" setStateChangeFieldsProperties ", () => {
    expect(FieldAction.setStateChangeFieldsProperties({ test: 1 }).payload.test).toBe(1)
    expect(FieldAction.setStateChangeFieldsProperties({ test: 1 }).type).toBe("ACTION_CHANGE_FIELDS_PROPERTIES")
  })
  test(" setStateChangeFieldsOrder ", () => {
    expect(FieldAction.setStateChangeFieldsOrder({ test: 1 }).payload.test).toBe(1)
    expect(FieldAction.setStateChangeFieldsOrder({ test: 1 }).type).toBe("ACTION_CHANGE_FIELDS_ORDER")
  })
});

describe(" Form Action ", () => {
  test(" setStateAddForm ", () => {
    expect(FormAction.setStateAddForm({ test: 1 }).payload.test).toBe(1)
    expect(FormAction.setStateAddForm({ test: 1 }).type).toBe("ACTION_ADD_FORM")
  })
});

describe(" Properties Action ", () => {
  test(" setStateAddProperties ", () => {
    expect(PropertiesAction.setStateAddProperties(1).payload).toBe(1)
    expect(PropertiesAction.setStateAddProperties(1).type).toBe("ACTION_PREVIEW_PROPERTIES")
  })
});