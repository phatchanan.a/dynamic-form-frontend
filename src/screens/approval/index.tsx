import React, { useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Paper,
  makeStyles,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";

import { httpService } from "../../services";
import { v4 as uuidv4 } from "uuid";
import RightArrowIcon from '@material-ui/icons/ArrowForwardIos';
import SendIcon from '@material-ui/icons/Send';
import { Chart } from "chart.js";
import ChartDataLabels from 'chartjs-plugin-datalabels';
import Layout from "../../components/FormLayout";
import { PaginationViewModel, SearchViewModel } from "../../model";
import { Search, initSearchParams } from "../../components/Search";
import { ProjectListViewModel } from "../../model/project";
import { theme } from "../../Theme/Theme";
import { Pagination } from "../../components/Pagination";

const useStyles = makeStyles((theme) => ({
  root: {
    // height: `calc(100vh - (8vh + ${12}px*0))`,
    height: `92vh`,
    display: "flex",
    flex: 1,
    flexDirection: "row",
    padding: 0,
  },
  panel: {
    marginRight: '0px !important',
    height: `calc(100% - ${theme.spacing(1.5)}px*2) !important`,
    minHeight: `calc(100% - ${theme.spacing(1.5)}px*2) !important`,
    display: 'flex',
    flexDirection: 'column',
  },
  chart: {
    minHeight: `calc(50% - ${theme.spacing(1.5)}px*1.5) !important`,
    height: `calc(50% - ${theme.spacing(1.5)}px*1.5) !important`,
  },
  tasklist: {
    // height: `calc(50% - ${theme.spacing(1.5)}px*1.5) !important`,
    height: `calc(100% - ${theme.spacing(1.5)}px*2) !important`,
  },
  paper: {
    color: theme.palette.text.secondary,
    margin: 12,
    display: "flex",
    flexDirection: "column",
    // alignItems: "center",
    background: "#fafafa",
    cursor: 'pointer',
  },
}));

interface TaskListViewModel {
  FormID: number,
  FormCount: number,
  FormName: string,
}

export const Approval = () => {
  const classes = useStyles();
  const history = useHistory();
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [curProj, setCurProj] = useState(null);
  const [projList, setProjList] = useState<ProjectListViewModel[]>([]);
  const [projPaging, setProjPaging] = useState<PaginationViewModel | undefined>();
  const [searchParams, setSearchParams] = useState<SearchViewModel>(initSearchParams({ order: { 'UpdateDate': 'desc' }, limit: 10 }));
  const [taskList, setTaskList] = useState<TaskListViewModel[]>([]);

  useEffect(() => {
    getFormDataList(searchParams);
  }, []);

  const getFormDataList = async (searchParams: {}) => {
    let { items, meta } = await httpService.get<ProjectListViewModel>(`inspectapproval/FindProjectApproval`, { params: searchParams });
    setProjList(items || []);
    setProjPaging(meta);
    if (items?.length !== 0) {
      handleListItemClick((items || [])[0].ProjectID, 0);
    }
  };

  const handleListItemClick = (projID, index) => {

    setCurProj(projID);
    httpService.get(`inspectapproval/FindProjectApprovalByID/${projID}`).then(({ item }) => {

      setSelectedIndex(index);
      setTaskList([]);
      if (!item?.Overview) return;

      setTaskList(item?.Overview.TaskList);
      // DoughnutChart(item?.Overview);
      // BarChart(itemList.slice(1));
    });

  };

  const DoughnutChart = (data: any) => {

    let labels = data.Value.map(e => {
      return e.ApproveStatus;
    });

    let datas = data.Value.map(e => {
      return Math.round((e.Percentage) * 100) / 100;
    });

    // var myChart = new Chart((document?.getElementById('myChart') as any).getContext('2d'), {
    var myChart = new Chart((document?.querySelector('#Overall') as any).getContext('2d'), {
      plugins: [ChartDataLabels],
      type: 'doughnut',
      data: {
        // labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        labels: labels,
        datasets: [{
          // label: '# of Votes',
          // data: [12, 19, 3, 5, 2, 0],
          data: datas,
          backgroundColor: [
            'rgba(255, 99, 132, 0.8)', //Red
            'rgba(54, 162, 235, 0.8)', //Blue
            'rgba(153, 102, 255, 0.8)', //Purple
            // 'rgba(255, 206, 86, 0.8)', //Yellow
            // 'rgba(75, 192, 192, 0.8)', //Green
            // 'rgba(255, 159, 64, 0.8)' //Orange
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)', //Red
            'rgba(54, 162, 235, 1)', //Blue
            'rgba(153, 102, 255, 1)', //Purple
            // 'rgba(255, 206, 86, 1)', //Yellow
            // 'rgba(75, 192, 192, 1)', //Green
            // 'rgba(255, 159, 64, 1)' //Orange
          ],
          borderWidth: 1
        }]
      },
      options: {
        plugins: {
          // Change options for ALL labels of THIS CHART
          datalabels: {
            color: 'rgba(0, 0, 0, 0.6)',
            // backgroundColor: 'rgba(200, 200, 200, 0.5)',
            backgroundColor: 'rgba(150, 150, 150, 0.4)',
            borderRadius: 10,
            padding: 8,
            textStrokeWidth: 1,
            align: "end",
            // anchor: "end",
            display: 'auto',
            formatter: function (value, context) {
              return context.chart.data.labels[context.dataIndex] + ': ' + value + '%';
            }
          }
        },
        layout: {
          padding: {
            bottom: theme.spacing(1)
          }
        },
        scales: {
          yAxes: [{
            display: false,
            stacked: false,
            gridLines: {
              display: false,
              color: "rgba(255,99,132,0.2)"
            },
            ticks: {
              beginAtZero: true
            }
          }],
          xAxes: [{
            display: false,
            gridLines: {
              display: false
            }
          }]
        },
      }
    });
  };



  return (
    <div className={classes.root}>
      <Grid item xs={3}>
        <Paper id="panel" className={`${classes.paper} ${classes.panel}`} elevation={5} style={{}}>
          <Layout title="Project List" appBarColor="primary"></Layout>

          <Search hideFilter justifyCenter column={[{ field: 'ProjectNo|ProjectName', label: 'Project No. | Project Name' }]}
            conditions={JSON.parse(searchParams.conditions || '[]')}
            style={{ paddingLeft: theme.spacing(1), paddingRight: theme.spacing(1), boxShadow: '0 8px 10px -5px rgba(0, 0, 0, 0.20)' }}
            onSearch={(data) => {
              let searchData = { ...searchParams, conditions: JSON.stringify(data), page: 1 };
              setSearchParams(searchData);
              getFormDataList(searchData);
            }}
          />
          {/* <Divider /> */}

          <div id="projList" style={{ flex: 1 }} data-scrollbar>
            <List component="span" >

              {projList.map((e, index) => {
                return (
                  <div key={index}>
                    <ListItem
                      button
                      divider
                      selected={selectedIndex === index}
                      onClick={(event) => handleListItemClick(e.ProjectID, index)}
                    >
                      <ListItemText primary={e.ProjectNo} style={{ width: '5ch' }}></ListItemText>
                      <Divider orientation="vertical" style={{ height: 28, margin: '0px 20px' }} />
                      <Typography style={{ width: '70%' }}>{e.ProjectName}</Typography>

                      <ListItemIcon style={{ minWidth: 0, marginRight: -10, position: 'absolute', right: 10 }}>
                        <RightArrowIcon />
                      </ListItemIcon>

                    </ListItem>
                  </div>
                );
              })}

            </List>
          </div>

          <Pagination
            paging={projPaging}
            onPageChange={(event, page) => getFormDataList({ ...searchParams, page: page })}
          />

        </Paper>

      </Grid>

      <Grid item xs={9} style={{}}>
        {/* {itemList.map((e: any, i) => {
          return (
            <Paper className={`${classes.paper} ${classes.chart}`} elevation={5}>
              <Layout title={e.Name} appBarColor="primary">
                <canvas id={e.ID} style={{ height: (i == 0 ? '50vh' : '30vh'), width: '80vw' }}></canvas>
              </Layout>
            </Paper>
          );
        })} */}

        {/* <Paper className={`${classes.paper} ${classes.chart}`} elevation={5}>
          <Layout title="Overview" appBarColor="primary"></Layout>
          <canvas key={uuidv4()} id='Overall' style={{
            minHeight: `calc(100% - (${50}px))`,
            height: `calc(100% - (${50}px))`,
            width: `100%`,
          }}></canvas>

        </Paper> */}

        <Paper className={`${classes.paper} ${classes.tasklist}`} elevation={5}>
          <Layout title="My task Review & Approve form" appBarColor="primary"></Layout>

          <div id="taskList" data-scrollbar>
            <List component="span">

              {taskList.map((e, index) => {
                return (
                  <div key={index}>
                    <ListItem
                      button
                      selected={selectedIndex === index}
                      onClick={(event) => history.push(`/approval/${curProj}/${e.FormID}`)}
                    >
                      {index + 1}.
                      <ListItemText primary={e.FormName} style={{ width: '5ch', paddingLeft: theme.spacing(1) }}></ListItemText>
                      <Divider orientation="vertical" style={{ height: 28, margin: '0px 20px' }} />
                      <Typography style={{ width: '30%' }}>{e.FormCount} form(s)</Typography>

                      <ListItemIcon style={{ minWidth: 0, marginRight: -10, position: 'absolute', right: 10 }}>
                        <SendIcon />
                      </ListItemIcon>

                    </ListItem>
                    <Divider />
                  </div>
                );
              })}
            </List>
          </div>

        </Paper>

        {/* <Paper className={`${classes.paper} ${classes.chart}`} elevation={5}>
          <Layout title="2222" appBarColor="secondary">
            <canvas id='tttt' style={{ height: '35vh', width: '80vw' }}></canvas>
          </Layout>
        </Paper> */}

      </Grid>
    </div>
  );
};
