import React, { useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Paper,
  Button,
  makeStyles,
  List,
  ListItem,
  TextField,
  Divider,
  IconButton, InputAdornment, TableBody, TableCell, Table, TableRow
} from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";

import { httpService } from "../../services";
import ApproveIcon from '@material-ui/icons/CheckCircle';
import RejectIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/CheckBox';
import UnCheckIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CommentIcon from '@material-ui/icons/Comment';
import { isNullOrUndefined } from "util";
import { BaseViewModel, PaginationViewModel, SearchViewModel } from "../../model";
import { initSearchParams } from "../../components/Search";
import { theme } from "../../Theme/Theme";
import { AppLayout } from "../../components/AppLayout";
import MsgDlg, { ResultDlg } from "../../components/MsgDlg";
import { Pagination } from "../../components/Pagination";
import { ClipLoader } from "react-spinners";
import { Img } from 'react-image';
import Dialog from "../../components/Dialog";
import { v4 as uuidv4 } from "uuid";

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiListItem-root.Mui-selected': {
      background: 'rgb(0 0 0 / 0.2)',
    },
    '& #layout-header': {
      minHeight: '5vh',
    }
  },
  apprTable: {
    width: '100%',
    '& td': {
      border: 0,
      padding: theme.spacing(0.5, 0),
      height: theme.spacing(2),
    },
    '& tr > td:nth-child(1)': {
      fontWeight: 'bold',
      minWidth: '12%',
      width: '12%',
    },
    '& tr > td:nth-child(2)': {
      width: '10%',
    },
    '& tr > td:nth-child(3)': {
      minWidth: '75%',
    },
  },
  approveList: {
    // height: `calc(100% - ${theme.spacing(1.5)}px*2) !important`,
    // minHeight: `calc(100% - ${theme.spacing(1.5)}px*2) !important`,
    // flexDirection: 'column',

    // flex: 1,
    // width: '100%',
    // textAlign: 'center',
  },
  paper: {
    // color: theme.palette.text.secondary,
    margin: theme.spacing(2),
    // padding: theme.spacing(2),
    display: "block",
    // flexDirection: "column",
    // alignItems: "center",
    background: "#fafafa",
    cursor: 'pointer',
  },
  btn_reject: {
    borderRadius: 22,
    padding: theme.spacing(0.5, 1.5, 0.5, 1),
    minWidth: 'auto',
    height: 'fit-content',
    background: theme.palette.error.main,
    color: "#FFF",
    "&:hover": {
      background: theme.palette.error.dark,
    },
    "& .MuiSvgIcon-root": {
      fontSize: 25,
    },
  },
  btn_approve: {
    borderRadius: 22,
    padding: theme.spacing(0.5, 1.5, 0.5, 1),
    minWidth: 'auto',
    height: 'fit-content',
    background: theme.palette.success.main,
    color: "#FFF",
    "&:hover": {
      background: theme.palette.success.dark,
    },
    "& .MuiSvgIcon-root": {
      fontSize: 25,
    },
  },
  paperImageList: {
    flexDirection: 'column',
    display: 'flex',
    height: '100px',
    width: '150px',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    border: '4px white solid',
    position: 'relative',
    margin: theme.spacing(0, 2, 2, 0),
  },
  inspImage: {
    width: 150,
    opacity: 0,
    transition: 'opacity 2500ms',
    position: 'absolute',
    "&:hover": {
      opacity: 0.6,
      background: theme.palette.success.dark,
    }
  },
  opa1: {
    opacity: 1,
  },
  trans100: {
    transition: 'opacity 100ms',
  }
}));

interface ApprListViewModel extends BaseViewModel {
  DrawingNo: string,
  InspectDetailList: { FieldKey: string, FieldLabel: string, FieldValue: any }[],
  InspectID: number,
  JointNo: string,
  LineNo: string,
  PackageNo: string,
}

interface ApprDataViewModel {
  FormCount?: number,
  FormID?: number,
  FormName?: string,
  ProjectID?: number,
  ProjectName?: string,
}

export const Approve = () => {
  const classes = useStyles();
  const params = useParams<any>();
  const history = useHistory();
  const [apprSelected, setApprSelected] = useState<{ [key: string]: { selected: boolean, remark: string } }>({});
  const [apprData, setApprData] = useState<ApprDataViewModel>({});
  const [apprList, setApprList] = useState<ApprListViewModel[]>([]);
  const [apprPaging, setApprPaging] = useState<PaginationViewModel | undefined>();
  // const [searchParams, setSearchParams] = useState<SearchViewModel>(initSearchParams({ order: { 'UpdateDate': 'desc' }, limit: 10 }));
  const searchParams: SearchViewModel = initSearchParams({ order: { 'UpdateDate': 'desc' }, limit: 10 });
  const [remarkAll, setRemarkAll] = useState('');
  const [preview, setPreview] = useState(null);
  const [uid, setUid] = useState(uuidv4());

  useEffect(() => {
    getApprDataList(searchParams);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps  

  const getApprDataList = async (searchParams: {}) => {
    let { item, items, meta } = await httpService.get<ApprListViewModel>(`inspectapproval/FindInspectByFormID/${params.ProjectID}/${params.FormID}`, { params: searchParams });
    if (items?.length === 0) {
      history.push("/approval")
    }

    setApprData(item as ApprDataViewModel);
    setApprList(items || []);
    setApprPaging(meta);

    let result = items.reduce((map, obj) => {
      map[obj.InspectID] = { selected: false, remark: "" };
      return map;
    }, {});

    setApprSelected(result);

    if (items?.length !== 0) {
      // handleListItemClick((items || [])[0].ProjectID, 0);
    }
  };

  const updateInspApproval = (action, remark, inspList) => {

    if (action === "Reject" && remark.trim() === '') {
      MsgDlg({
        status: "error",
        message: "Please add your comment before reject action.",
      });
      return;
    }

    let data = {
      // ApprovalLevelID: parseInt(params.ApprovalLevelID),
      ProjectID: parseInt(params.ProjectID),
      Action: action,
      Remark: remark,
      // ApproveBy: "Dev",
      InspList: inspList,
    };

    httpService.put(`inspectapproval/UpdateInspApproval`, data).then((response) => {
      ResultDlg(response, {
        onClose: () => {
          getApprDataList({ ...searchParams, page: 1 });
          setRemarkAll('');
        }
      });
    });
  }

  useEffect(() => {
    console.log(apprSelected);
  }, [apprSelected])

  useEffect(() => {
    if (history.location.pathname.indexOf('/approval') === 0)
      document.body.appendChild(document.getElementById("apprPanel"));
  }, [document.getElementById("apprPanel")]) // eslint-disable-line react-hooks/exhaustive-deps  

  const blobToBase64 = blob => {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    return new Promise(resolve => {
      reader.onloadend = () => {
        resolve(reader.result);
      };
    });
  };

  const getImage = async (inspListID) => {

    let blob = await httpService.getImage('image', { params: { InspListID: inspListID } });

    // return await blobToBase64(blob) as string;
    return URL.createObjectURL(blob);
  }

  return (
    <AppLayout className={`${classes.root}`}>
      <>
        <Grid item xs={6}>
          <Typography component="h6" variant="h6" style={{ display: 'flex' }} color="textPrimary">

            {apprData?.ProjectName}
            {apprData?.FormName ?
              <>
                <Divider className='divider-header' orientation="vertical" style={{ width: 2, height: 22, margin: theme.spacing(0.6, 2, 0, 2) }} />
                {apprData.FormName}
              </> : ''}

          </Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography align="right">
            <IconButton onClick={() => history.push('/approval')}>
              <CloseIcon />
            </IconButton>
          </Typography>
        </Grid>
      </>

      <Paper style={{}}>

        {preview && <Dialog image openDlg={preview !== null} onClose={() => { setPreview(null) }} title={'Preview'}>
          <img src={(document.getElementById(`${preview}`) as HTMLImageElement).src} alt="" style={{ maxHeight: '80vh' }} />
        </Dialog>}

        <div style={{ width: '50%', flex: 1 }}>
          {apprList.map((list, index) => {
            const selected = apprSelected ? apprSelected[list.InspectID]?.selected : false;
            const remark = apprSelected ? apprSelected[list.InspectID]?.remark || '' : '';
            return (
              <Paper key={index} className={`${classes.paper}`} elevation={5}>
                <ListItem
                  button
                  selected={selected}
                  onClick={(event) => {
                    setApprSelected({
                      ...apprSelected,
                      [list.InspectID]: {
                        selected: !apprSelected[list.InspectID].selected
                      }
                    });
                  }}
                >

                  <List component="div" style={{ width: '100%' }}>

                    <Table className={`${classes.apprTable}`}>
                      <TableBody>
                        <TableRow>
                          <TableCell>{"Line No."}</TableCell>
                          <TableCell><Divider orientation="vertical" style={{ height: '100%', margin: '0px 30px' }} /></TableCell>
                          <TableCell>{list.LineNo}</TableCell>
                        </TableRow>

                        <TableRow>
                          <TableCell>{"Drawing No."}</TableCell>
                          <TableCell><Divider orientation="vertical" style={{ height: '100%', margin: '0px 30px' }} /></TableCell>
                          <TableCell>{list.DrawingNo}</TableCell>
                        </TableRow>

                        {list.JointNo &&
                          <TableRow>
                            <TableCell>{"Joint No."}</TableCell>
                            <TableCell><Divider orientation="vertical" style={{ height: '100%', margin: '0px 30px' }} /></TableCell>
                            <TableCell>{list.JointNo}</TableCell>
                          </TableRow>}

                        {list.InspectDetailList.map((insp, j) => {

                          return <TableRow key={j}>
                            <TableCell>{insp.FieldLabel}</TableCell>
                            <TableCell><Divider orientation="vertical" style={{ height: '100%', margin: '0px 30px' }} /></TableCell>
                            {insp?.FieldKey !== "image" && <TableCell>{insp.FieldValue}</TableCell>}
                            {insp?.FieldKey === "image" && <TableCell>

                              <Grid container>

                                {insp.FieldValue.map((val, k) => {

                                  return <Grid item xs={4} key={k}>
                                    <Paper className={classes.paperImageList} id={`imageInsp${val.InspListID}`} elevation={4}>
                                      <div id='loader' style={{ position: 'absolute' }}>
                                        <ClipLoader size={40} css={'border: 6px ' + theme.palette.primary.main + ' solid;border-bottom-color: transparent; transition: opacity 2000ms; opacity: 1'} />
                                      </div>

                                      <Img src={`insp${val.InspListID}${uid}`} imgPromise={async (args) => {
                                        getImage(val.InspListID).then(blob => {
                                          let imgElem = document.getElementById(`imageInsp${val.InspListID}`);
                                          let img = (imgElem.querySelector('img') as HTMLImageElement);

                                          setTimeout(() => {
                                            (imgElem.querySelector('div#loader') as HTMLElement).style.opacity = '0';
                                            imgElem.querySelector('div#loader').remove();
                                          }, 500);

                                          img.src = blob;
                                          img.classList.add(classes.opa1);

                                          setTimeout(() => {
                                            img.classList.add(classes.trans100);
                                          }, 2500);

                                        });
                                      }}

                                        container={(children) => {
                                          return <img className={classes.inspImage} id={`image${val.InspListID}`} alt="" onClick={(e) => {
                                            e.stopPropagation();
                                            setPreview(`image${val.InspListID}`);
                                          }} />
                                        }}
                                      />

                                    </Paper>
                                  </Grid>
                                })}
                              </Grid>
                            </TableCell>}
                          </TableRow>
                        }

                        )}

                      </TableBody>
                    </Table>

                  </List>
                  <div style={{ position: 'absolute', top: 5, right: 5 }}>
                    {selected ? <CheckIcon style={{ fontSize: 40 }} /> : <UnCheckIcon style={{ fontSize: 40 }} />}
                  </div>
                </ListItem>

                <Divider style={{ marginBottom: theme.spacing(0.5) }} />
                <div style={{ padding: theme.spacing(0.5, 1), display: 'flex' }}>
                  <TextField
                    // onKeyUp={(e) => onSearchChange(e)}
                    // label="Comment"
                    disabled={selected}
                    placeholder="Please add your comment"
                    margin="dense"
                    variant="standard"
                    fullWidth
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start" >
                          <CommentIcon />
                        </InputAdornment>
                      )
                    }}
                    value={remark}
                    onChange={(e) => {
                      setApprSelected({
                        ...apprSelected,
                        [list.InspectID]: {
                          remark: e.target.value
                        }
                      });
                    }}
                  />

                  <Button
                    variant="contained"
                    startIcon={<ApproveIcon />}
                    className={`${classes.btn_approve}`}
                    disabled={selected}
                    onClick={() => {
                      updateInspApproval("Approve", remark, [{ InspID: list.InspectID }]);
                    }}
                  >
                    Approve
                  </Button>

                  <Button
                    variant="contained"
                    startIcon={<RejectIcon />}
                    style={{ marginLeft: theme.spacing(1) }}
                    className={`${classes.btn_reject}`}
                    disabled={selected}
                    onClick={() => {
                      updateInspApproval("Reject", remark, [{ InspID: list.InspectID }]);
                    }}
                  >
                    Reject
                  </Button>

                </div>
              </Paper>
            );
          })}
        </div>

        <Pagination
          style={{ width: '50%' }}
          paging={apprPaging}
          onPageChange={(event, page) => getApprDataList({ ...searchParams, page: page })}
        />
        <div style={{ height: '34px' }}></div>
      </Paper>

      <Paper id="apprPanel" elevation={5} square style={{
        padding: theme.spacing(1),
        margin: theme.spacing(1.5),
        position: 'fixed', bottom: -15,
        width: `calc(100% - ${theme.spacing(3)}px)`,
        display: 'flex',
        justifyContent: 'center',
        boxShadow: '0 -8px 10px -5px rgba(0, 0, 0, 0.20)'
      }}>
        <div style={{ padding: theme.spacing(0.5, 1), width: '60%', display: 'flex', background: '#fff' }}>

          <TextField
            placeholder="Please add your comment"
            margin="dense"
            variant="standard"
            fullWidth
            disabled={Object.keys(apprSelected)?.map(inspID => apprSelected[inspID]).filter(list => list.selected).length === 0}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start" >
                  <CommentIcon />
                </InputAdornment>
              )
            }}
            value={remarkAll}
            onChange={(e) => {
              setRemarkAll(e.target.value);
            }}
          />

          <Button
            variant="contained"
            startIcon={<ApproveIcon />}
            className={`${classes.btn_approve}`}
            disabled={Object.keys(apprSelected)?.map(inspID => apprSelected[inspID]).filter(list => list.selected).length === 0}
            onClick={() => {

              let inspList = apprList.map((list, index) => {
                if (apprSelected[list.InspectID].selected) return { InspID: list.InspectID };
                else return null;
              }).filter(list => !isNullOrUndefined(list));

              updateInspApproval("Approve", remarkAll, inspList);

            }}
          >
            Approve
          </Button>

          <Button
            variant="contained"
            startIcon={<RejectIcon />}
            style={{ marginLeft: theme.spacing(1) }}
            className={`${classes.btn_reject}`}
            disabled={Object.keys(apprSelected)?.map(inspID => apprSelected[inspID]).filter(list => list.selected).length === 0}
            onClick={() => {

              let inspList = apprList.map((list, index) => {
                if (apprSelected[list.InspectID].selected) return { InspID: list.InspectID };
                else return null;
              }).filter(list => !isNullOrUndefined(list));

              updateInspApproval("Reject", remarkAll, inspList);

            }}
          >
            Reject
          </Button>

        </div>

      </Paper>

    </AppLayout >
  );
};
