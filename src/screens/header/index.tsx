import {
  AppBar,
  Button,
  createStyles,
  IconButton,
  List,
  ListItem,
  makeStyles,
  MenuList,
  Theme,
  Toolbar,
  Typography
} from "@material-ui/core";
import Drawer from '@material-ui/core/Drawer';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Menu } from "@material-ui/icons";
import Logout from "@material-ui/icons/ExitToApp";
// import { useKeycloak } from "@react-keycloak/web";
// import { KeycloakInstance } from "keycloak-js";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, withRouter } from "react-router-dom";
import Scrollbar from 'smooth-scrollbar';
import * as calAction from "../../actions/cal.action";
import { IinitialState } from "../../reducers/reducers.interface";
import Routes from '../../routes/RouteMenu';
import "./index.sass";
import Collapse from "@material-ui/core/Collapse";

import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { Link } from 'react-router-dom';
import userManager from "../../userManager";
import ShowLoadingScreen from "../../components/LoadingScreen";
import { ResetPassword } from "../login/ResetPassword";
import VpnKeyIcon from '@material-ui/icons/VpnKey';

// Scrollbar.use(OverscrollPlugin);
export const Scrollbar_ = Scrollbar;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    // '@global': {
    //   '*::-webkit-scrollbar': {
    //     width: '0.3em',
    //   },
    //   '*::-webkit-scrollbar-track': {
    //     boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
    //     webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
    //   },
    //   '*::-webkit-scrollbar-track-piece': {
    //     background: '#F0F0F0',
    //   },
    //   '*::-webkit-scrollbar-thumb': {
    //     background: '#bcc6d8',
    //     outline: '1px solid slategrey',
    //     borderRadius: 5,
    //     // transition: 'background-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out',
    //   },
    // },
    root: {
      display: "flex",
      flexDirection: "column",
      height: "100vh",
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    fullList: {
      width: 'auto',
    },
    textDrawer: {
      fontSize: '120%',
      textShadow: '1px 1px 2px rgba(0, 0, 0, 0.5)',
    },
    drawerSize: {
      width: '15%',
    },
    hidden: {
      display: 'none',
    }
  })
);

const Header = (props: any) => {
  const { setCurUser, curUser } = props;
  const classes = useStyles();
  const history = useHistory();
  const [state, setState] = React.useState(false);
  // const { keycloak } = useKeycloak<KeycloakInstance>();
  const [menuSelectedIndex, setMenuSelectedIndex] = React.useState(-1);
  const [openExpand, setOpenExpand] = React.useState(false);

  useEffect(() => {
    Scrollbar.init(document.body);
    Scrollbar.initAll();

    if (history.location.pathname.indexOf('/approval/') !== 0 && document.getElementById("apprPanel"))
      document.body.removeChild(document.getElementById("apprPanel"));

    document.body.appendChild(document.getElementById('SpecialDiv'));

    // setTimeout(() => {
    //   if (localStorage.getItem('curUser')) {
    //     setCurUser(JSON.parse(localStorage.getItem('curUser')));
    //   }
    // }, 500);

  }, [history.location]);

  const activeRoute = (routeName: any) => {
    return props.location.pathname === routeName ? true : false;
  }

  const toggleDrawer = (open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent,
  ) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' ||
        (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }

    setState(open);
  };

  const logout = () => {

    ShowLoadingScreen();

    let userType = localStorage.getItem('userType');
    // localStorage.removeItem('accessTokens');
    // localStorage.removeItem('curUser');
    // localStorage.removeItem('userType');
    localStorage.clear();
    localStorage.setItem('redirectURL', window.location.pathname);

    setTimeout(() => {
      ShowLoadingScreen(false);
      if (userType === "ADFS") userManager.signoutRedirect();
      else if (userType === "local") {
        setCurUser(null);
        history.push('/login');
      }
    }, 800);

    // userManager.userSignedOut().then(()=>{
    //   history.push('/');
    // });
    // keycloak.logout().then(() => {
    //   keycloak.clearToken();
    //   history.push('/')
    // });
  };

  const changePass = () => {
    history.push('/resetpassword');
  }

  return (
    <div>
      <Drawer classes={{ paper: classes.drawerSize }} anchor={'left'} open={state} onClose={toggleDrawer(false)} >
        <div
          className={classes.fullList}
          role="presentation"
          //onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
        >
          <MenuList>
            <List component="span" >
              {/* ********************************************************************* */}
              {/* {curUser && Routes.filter(menu => curUser?.MenuList.filter(mlist => mlist === menu.role).length !== 0).map((prop, index) => { */}
              {Routes.map((prop, index) => {
                return (
                  <div key={index}>
                    <ListItem button component={prop.subRoute.length === 0 ? Link : null} to={prop.path} key={prop.sidebarName} style={{ textDecoration: 'none', paddingLeft: 30 }} selected={activeRoute(prop.path)}
                      onClick={(e) => {
                        if (prop.subRoute.length === 0) setState(false);
                        setMenuSelectedIndex(index);
                        setOpenExpand(!openExpand);
                      }}
                    >
                      <ListItemIcon>{prop.icon}</ListItemIcon>
                      <ListItemText classes={{ primary: classes.textDrawer }} primary={prop.sidebarName} />

                      {prop.subRoute.length > 0 &&
                        <ListItemIcon style={{ minWidth: 0, marginRight: -10, position: 'absolute', right: 10 }}>
                          {(openExpand && menuSelectedIndex === index) ? <ExpandLess /> : <ExpandMore />}
                          {/* <RightArrowIcon /> */}
                        </ListItemIcon>
                      }

                    </ListItem>
                    <Collapse in={prop.subRoute.length > 0 && openExpand && menuSelectedIndex === index} timeout="auto" unmountOnExit>
                      <List component="span">
                        {prop.subRoute.map((item, i) => {
                          return (
                            <ListItem button component={Link} to={item.path} key={index} onClick={toggleDrawer(false)} style={{ paddingLeft: 60 }} selected={activeRoute(item.path)}>

                              <ListItemIcon>{item.icon}</ListItemIcon>
                              <ListItemText secondary={item.sidebarName} />
                            </ListItem>
                          );
                        })
                        }
                      </List>

                    </Collapse>
                  </div>
                );
              })}
            </List>
          </MenuList>
        </div>
      </Drawer>

      <AppBar id="layout-header-bar" position="static">
        <Toolbar style={{ minHeight: '8vh' }}>
          <IconButton
            edge="start"
            // className={localStorage.getItem('accessTokens') ? classes.menuButton : classes.hidden}******************************************
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={toggleDrawer(true)}

          >
            <Menu />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Camunda Workflow System
          </Typography>

          {(localStorage.getItem('userType') === "local") && <IconButton
            edge="start"
            onClick={changePass}
            color="inherit"
            title="Change Password"
          >
            <VpnKeyIcon />
          </IconButton>
          }

          {curUser?.UserName}
          {curUser && <IconButton id="logout"
            edge="end"
            onClick={logout}
            color="inherit"
            title="Logout"
          >
            <Logout />
          </IconButton>}

          {/* <Button color="inherit" onClick={logout}>Logout</Button> */}

        </Toolbar>
      </AppBar>
      <div id="SpecialDiv">
        <div id="MsgDlg" style={{ position: 'absolute' }}></div>
        <div id="CusDlg" style={{ position: 'absolute' }}></div>
        <div id="Loading" style={{ height: 0 }}></div>
      </div>
      {/* <div id="MsgDlg" style={{ position: 'absolute' }}></div>
      <div id="Dlg" style={{ position: 'absolute' }}></div>
      <div id="Loading" style={{ height: 0 }}></div> */}
    </div >
  );
};

export default withRouter(Header);