import React, { useState, useEffect, ChangeEvent } from "react";
import {
  Checkbox,
  FormControlLabel,
  Grid,
  Typography,
} from "@material-ui/core";
import { useHistory } from 'react-router-dom';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { httpService } from '../../../services';
import TextField from '@material-ui/core/TextField';

import { isNullOrUndefined } from "util";
import Dialog from "../../../components/Dialog";
import { ResultDlg } from "../../../components/MsgDlg";
import { UserViewModel } from ".";
import { theme } from "../../../Theme/Theme";

export const UserDialog = (props: any) => {
  const { getUserProfileDatalist, searchParams, listProfile, openDlg, setOpenDlg, dataEdit } = props;
  const history = useHistory();
  const [userProfileData, setUserProfileData] = useState<UserViewModel>({});
  const [submitted, setSubmitted] = useState(false);
  const [profileIndex, setProfileIndex] = useState(-1);
  // const [openDlg, setOpenDlg] = useState(true);

  useEffect(() => {
    setSubmitted(false);
    if (isNullOrUndefined(dataEdit)) {
      setProfileIndex(-1);
      setUserProfileData({});
    }
    else {
      setUserProfileData(dataEdit);
      setProfileIndex(listProfile.map((d: any) => d.ProfileID).indexOf(dataEdit.ProfileID));
    }
  }, [openDlg]);

  const handleSave = async () => {
    setSubmitted(true);

    if (!checkValidateError()) {
      if (!dataEdit)
        await httpService.post(`userprofile`, userProfileData).then((response) => {

          if (response.result) setOpenDlg(false);
          ResultDlg(response, {
            onClose: () => {
              if (response.result) {
                getUserProfileDatalist(searchParams);
                history.push("/admin/user");
              }
            }
          });
        });
      else
        await httpService.put(`userprofile/${dataEdit.UserID}`, userProfileData).then((response) => {
          if (response.result) setOpenDlg(false);
          ResultDlg(response, {
            onClose: () => {
              if (response.result) {
                getUserProfileDatalist(searchParams);
                history.push("/admin/user");
              }
              // if (response.result) history.push("/project/" + response.items.ProjectID);
            }
          });
        });
    }
  };

  const checkValidateError = () => {
    if ((isNullOrUndefined(userProfileData.UserName)) ||
      (isNullOrUndefined(userProfileData.Email)) ||
      (isNullOrUndefined(userProfileData.ProfileID) || profileIndex === -1) ||
      (isNullOrUndefined(userProfileData.Password) && userProfileData.IsLocalUser && !dataEdit) ||
      ((!(userProfileData.UserName?.length >= 5 && userProfileData.UserName?.length <= 50)) && !dataEdit) ||
      ((!(userProfileData.Email?.length >= 5 && userProfileData.Email?.length <= 50)) && !dataEdit) ||
      ((!(userProfileData.Password?.length >= 5 && userProfileData.Password?.length <= 50)) && !dataEdit && userProfileData.IsLocalUser) ||
      ((!(userProfileData.Password?.length >= 5 && userProfileData.Password?.length <= 50)) && dataEdit !== null && !(isNullOrUndefined(userProfileData.Password)))
    ) {
      return true;
    } else {
      return false;
    }
  }

  return (
    openDlg &&
    <Dialog
      openDlg={openDlg}
      setOpenDlg={setOpenDlg}
      title={`${isNullOrUndefined(dataEdit) ? 'Set User Profile' : 'Edit User Profile'}`}
      onSubmit={{ text: `${isNullOrUndefined(dataEdit) ? 'Create' : 'Save'}`, onClick: handleSave, autoClose: false }}
      // onClose={handleClose}
      maxWidth='sm'
    // customButton={[
    //   { text: `${projID ? 'Save' : 'Create'}`, buttonStyle: 'btn_ok', onClick: () => { handleSave() }, autoClose: false },
    //   { text: 'Cancel', buttonStyle: 'btn_cancel', onClick: () => { } },
    // ]}
    >
      <Grid container direction="column">
        <Grid item xs>
          <form noValidate>
            <TextField
              required
              label="User Name"
              variant="outlined"
              margin="dense"
              fullWidth
              autoFocus
              value={userProfileData.UserName}
              disabled={dataEdit}
              helperText={
                (submitted && ((!(userProfileData.UserName?.length >= 5 && userProfileData.UserName?.length <= 50)) && !dataEdit) || ((!(userProfileData.Password?.length >= 5 && userProfileData.Password?.length <= 50)) && dataEdit !== null && (!isNullOrUndefined(userProfileData.Password)))) ?
                  "Must have 5 character more than or equal" : ""
              }
              onChange={(e) => {
                setUserProfileData({ ...userProfileData, UserName: e.target.value })
              }}
              // error={submitted && (!userProfileData.UserName || userProfileData.UserName === '')}
              error={
                submitted && (((!(userProfileData.UserName?.length >= 5 && userProfileData.UserName?.length <= 50)) && !dataEdit))

              }
            />

            <TextField
              required
              label="Email"
              variant="outlined"
              margin="dense"
              fullWidth
              value={userProfileData.Email}
              disabled={dataEdit}
              helperText={
                (submitted && ((!(userProfileData.Email?.length >= 5 && userProfileData.Email?.length <= 50)) && !dataEdit)) ?
                  "Must have 5 character more than or equal" : ""
              }
              onChange={(e) => {
                setUserProfileData({ ...userProfileData, Email: e.target.value })
              }}
              //error={submitted && (!userProfileData.Email || userProfileData.Email === '')}
              error={
                submitted && (((!(userProfileData.Email?.length >= 5 && userProfileData.Email?.length <= 50)) && !dataEdit))
              }
            />

            <Autocomplete
              id="Profile"
              options={listProfile}
              getOptionLabel={(option: any) => option.ProfileName}
              value={(profileIndex >= 0 ? listProfile[profileIndex] : null)}
              onChange={(e: any, data: any | null) => {
                setProfileIndex(listProfile.indexOf(data));
                setUserProfileData({ ...userProfileData, ProfileID: data ? parseInt(String(data.ProfileID)) : 0 })
              }}
              renderInput={(params) => <TextField {...params} label="Profile" variant="outlined" margin="dense"
                error={submitted && (isNullOrUndefined(userProfileData.ProfileID) || profileIndex === -1)} required />}
              renderOption={(option) => <Typography noWrap>{option.ProfileName}</Typography>}
            />

            <div style={{ display: 'block', marginTop: theme.spacing(0.5) }}>
              <FormControlLabel
                style={{ lineHeight: theme.spacing(1.5) }}
                control={
                  <Checkbox
                    color="primary"
                    disabled={dataEdit}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                      setUserProfileData({ ...userProfileData, IsLocalUser: e.target.checked, Password: null })
                    }}
                    checked={userProfileData.IsLocalUser}
                  />
                }
                label={
                  <Typography>
                    Local User
                  </Typography>
                }
              />
            </div>
            <TextField
              required={userProfileData.IsLocalUser && !dataEdit}
              type="password"
              label="Password"
              variant="outlined"
              margin="dense"
              helperText={
                (submitted && ((!(userProfileData.Password?.length >= 5 && userProfileData.Password?.length <= 50)) && !dataEdit) || ((!(userProfileData.Password?.length >= 5 && userProfileData.Password?.length <= 50)) && dataEdit !== null && (!isNullOrUndefined(userProfileData.Password)))) ?
                  "Must have 5 character more than or equal" : ""
              }
              fullWidth
              value={userProfileData.Password || ''}
              disabled={!(userProfileData.IsLocalUser)}
              onChange={(e) => {
                setUserProfileData({ ...userProfileData, Password: e.target.value })
              }}
              error={
                submitted //&& ((!userProfileData.Password || userProfileData.Password === '') && !dataEdit)
                //&& ((!(userProfileData.Password?.length >= 5 && userProfileData.Password?.length <= 50) || !(!userProfileData.Password || userProfileData.Password === '')) && dataEdit)
                && (((!(userProfileData.Password?.length >= 5 && userProfileData.Password?.length <= 50)) && !dataEdit && userProfileData.IsLocalUser) || ((!(userProfileData.Password?.length >= 5 && userProfileData.Password?.length <= 50)) && dataEdit !== null && (!isNullOrUndefined(userProfileData.Password))))

              }
            />
          </form>
        </Grid>
      </Grid>
    </Dialog>

  );
};
