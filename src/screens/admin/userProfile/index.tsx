import React, { useState, useEffect } from "react";
import {
  Grid,
  Button,
  IconButton,
  Paper,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from '@material-ui/icons/Delete';
import { httpService } from '../../../services';

import DataTable, { Column } from '../../../components/DataTable'
import { ConfirmDlg, ResultDlg } from "../../../components/MsgDlg";
import { BaseViewModel, PaginationViewModel, SearchViewModel } from "../../../model/form";
import { Search, initSearchParams } from "../../../components/Search";
import { AppLayout } from "../../../components/AppLayout";
import EditIcon from '@material-ui/icons/Edit';
import { UserDialog } from "./dialogUser";

// const useStyles = makeStyles((theme: Theme) =>
//   createStyles({

//   })
// );

export interface UserViewModel extends BaseViewModel {
  UserID?: number,
  UserName?: string,
  Password?: string,
  IsLocalUser?: boolean,
  Email?: string,
  ProfileID?: number,
  ProfileName?: string,
}

interface ProfileModel extends BaseViewModel {
  ProfileID?: number,
  ProfileName?: string,
}

export const UserProfile = () => {
  const [openDlg, setOpenDlg] = useState(false);
  const [userProfileList, setUserProfileList] = useState<UserViewModel[]>([]);
  const [projectPaging, setProjectPaging] = useState<PaginationViewModel | undefined>();
  const [searchParams, setSearchParams] = useState<SearchViewModel>(initSearchParams({ order: { UserName: 'desc' } }));
  const [listProfile, setListProfile] = useState<ProfileModel[]>([]);
  const [data, setData] = useState<UserViewModel>()
  const [eventClickEdit, setEventClickEdit] = useState(false);

  useEffect(() => {
    getUserProfileDatalist(searchParams);
    httpService.get(`profile`).then(response => {
      setListProfile(response.items);
    })

  }, []);

  const getUserProfileDatalist = async (searchParams: {}) => {
    let { items, meta } = await httpService.get<UserViewModel>(`userprofile`, { params: searchParams });
    setUserProfileList(items || []);
    setProjectPaging(meta);
  }

  async function handleDeleteUserProfileByID(deleteProjectID) {
    await httpService.delete(`userprofile/${deleteProjectID}`).then(response => {
      ResultDlg(response);
      getUserProfileDatalist(searchParams);
    });
  }

  const projectRoleListColumns: Column<UserViewModel>[] = [
    {
      field: 'UserName', label: 'User Name',
    },
    { field: 'Email', label: 'Email' },
    { field: 'ProfileName', label: 'Profile' },
    {
      field: 'Action', label: 'Action', headerProps: { align: 'center' }, searching: false, ordering: false,
      render: (row) => {
        return (
          <>
            <IconButton
              onClick={(e) => {
                e.stopPropagation();
                setEventClickEdit(true)
                setData(row);
                setOpenDlg(true)
              }}>
              <EditIcon />
            </IconButton>

            <IconButton
              onClick={(e) => {
                e.stopPropagation();
                ConfirmDlg({
                  message: 'You want to delete this item ?',
                  confirmButtonText: 'Yes, delete it!',
                  onConfirm: () => {
                    handleDeleteUserProfileByID(row.UserID);
                  },
                });
              }}>
              <DeleteIcon />
            </IconButton>
          </>
        );
      }
    },
  ];

  return (
    <AppLayout>
      <>
        <Grid item xs={3}>
          <Button
            // className={classes.button}
            variant="outlined"
            size="large"
            color="primary"
            startIcon={<AddIcon />}
            onClick={() => {
              setEventClickEdit(false);
              setOpenDlg(true);
            }}
          >
            Create
          </Button>
        </Grid>
        <Grid item xs={9}>

        </Grid>
      </>

      <Paper style={{ display: 'block' }}>
        <Search column={projectRoleListColumns} conditions={JSON.parse(searchParams.conditions || '[]')}
          onSearch={(data) => {
            let searchData = { ...searchParams, conditions: JSON.stringify(data), page: 1 };
            setSearchParams(searchData);
            getUserProfileDatalist(searchData);
          }}
        />
        <DataTable data={userProfileList} columns={projectRoleListColumns}
          searchParams={searchParams} paging={projectPaging}
          onRender={(searchParams) => {
            setSearchParams(searchParams);
            getUserProfileDatalist(searchParams);
          }
          }
        />
      </Paper>

      {openDlg && <UserDialog openDlg={openDlg} setOpenDlg={setOpenDlg} listProfile={listProfile} getUserProfileDatalist={getUserProfileDatalist} searchParams={searchParams} dataEdit={eventClickEdit ? data : null} />}
    </AppLayout >
  );
};
