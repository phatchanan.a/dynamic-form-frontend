import React, { useState, useEffect } from "react";
import {
  Grid,
  Typography,
} from "@material-ui/core";
import { useHistory } from 'react-router-dom';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { httpService } from '../../../services';
import TextField from '@material-ui/core/TextField';

import { isNullOrUndefined } from "util";
import Dialog from "../../../components/Dialog";
import { ResultDlg } from "../../../components/MsgDlg";
import { ApprovalModel, ProjectModel, UserModel } from ".";

interface RequestViewModel {
  UserID?: number,
  ProjectID?: number,
  ApprovalLevelID?: number,
}

export const RoleDialog = (props: any) => {
  const { getRoleDatalist, searchParams, listUser, listProject, listApproval, openDlg, setOpenDlg, userData } = props;
  const [roleData, setRoleData] = useState<RequestViewModel>({});
  const [submitted, setSubmitted] = useState(false);
  const [userIndex, setUserIndex] = useState(-1);
  const [projectIndex, setProjectIndex] = useState(-1);
  const [approveIndex, setApproveIndex] = useState(-1);
  // const [openDlg, setOpenDlg] = useState(true);

  useEffect(() => {
    setSubmitted(false);
    if (isNullOrUndefined(userData)) {
      setUserIndex(-1);
      setProjectIndex(-1);
      setApproveIndex(-1)
      setRoleData({});
    }
    else {
      setRoleData({
        UserID: userData.UserID,
        ProjectID: userData.ProjectID,
        ApprovalLevelID: userData.ApprovalLevelID,
      });
      setUserIndex(listUser.map((d: any) => d.UserID).indexOf(userData.UserID));
      setProjectIndex(listProject.map((d: any) => d.ProjectID).indexOf(userData.ProjectID));
      setApproveIndex(listApproval.map((d: any) => d.ApprovalLevelID).indexOf(userData.ApprovalLevelID));
    }
  }, [openDlg]);

  const handleSave = async () => {
    setSubmitted(true);
    if (!checkValidateError()) {
      if (!userData)
        await httpService.post(`projectRole/CreateProjectRole`, roleData).then((response) => {
          if (response.result) {
            getRoleDatalist(searchParams);
            setOpenDlg(false);
          }
          ResultDlg(response);
        });
      else
        await httpService.put(`projectRole/UpdateProjectRole/${userData.ProjectRoleID}`, roleData).then((response) => {
          if (response.result) {
            getRoleDatalist(searchParams);
            setOpenDlg(false);
          }
          ResultDlg(response);
        });
    }
  };

  const checkValidateError = () => {
    if ((isNullOrUndefined(roleData.UserID) || userIndex === -1) ||
      (isNullOrUndefined(roleData.ProjectID) || projectIndex === -1) ||
      (isNullOrUndefined(roleData.ApprovalLevelID) || approveIndex === -1)
    ) {
      return true;
    } else {
      return false;
    }
  }

  return (
    openDlg &&
    <Dialog
      openDlg={openDlg}
      setOpenDlg={setOpenDlg}
      title={`${isNullOrUndefined(userData) ? 'Set Project Role' : 'Edit Project Role'}`}
      onSubmit={{
        text: `${isNullOrUndefined(userData) ? 'Create' : 'Save'}`, onClick: handleSave, autoClose: false,
        disabled: userData ? (userData?.ApprovalLevelID === roleData?.ApprovalLevelID && userData?.ProjectID === roleData?.ProjectID) : false
      }}
      // onClose={handleClose}
      maxWidth='sm'
    // customButton={[
    //   { text: `${projID ? 'Save' : 'Create'}`, buttonStyle: 'btn_ok', onClick: () => { handleSave() }, autoClose: false },
    //   { text: 'Cancel', buttonStyle: 'btn_cancel', onClick: () => { } },
    // ]}
    >
      <Grid container direction="column">
        <Grid item xs>
          <form noValidate>

            <Autocomplete
              id="User"
              disabled={isNullOrUndefined(userData) ? false : true}
              options={listUser}
              getOptionLabel={(option: UserModel) => option.UserName}
              value={(userIndex >= 0 ? listUser[userIndex] : null)}
              onChange={(e, data: any) => {
                setUserIndex(listUser.indexOf(data));
                setRoleData({ ...roleData, UserID: data ? parseInt(String(data.UserID)) : 0 })
              }}
              renderInput={(params) => <TextField {...params} label="User Name" variant="outlined" margin="dense"
                error={submitted && (isNullOrUndefined(roleData.UserID) || userIndex === -1)} required />}
              renderOption={(option) => <Typography noWrap>{option.UserName}</Typography>}
            />

            <Autocomplete
              id="Role"
              options={listApproval}
              getOptionLabel={(option: ApprovalModel) => option.ApprovalLevel}
              value={(approveIndex >= 0 ? listApproval[approveIndex] : null)}
              onChange={(e, data: any) => {
                setProjectIndex(-1);
                setApproveIndex(listApproval.indexOf(data));
                setRoleData({ ...roleData, ApprovalLevelID: data ? parseInt(String(data.ApprovalLevelID)) : 0 });
              }}
              renderInput={(params) => <TextField {...params} label="Role" variant="outlined" margin="dense"
                error={submitted && (isNullOrUndefined(roleData.ApprovalLevelID) || approveIndex === -1)} required />}
              renderOption={(option) => <Typography noWrap>{option.ApprovalLevel}</Typography>}
            />

            <Autocomplete
              id="Project"
              disabled={approveIndex === -1}
              options={listProject}
              getOptionDisabled={(option: ProjectModel) =>
                option.ProjectID === 0 && (
                  listApproval[approveIndex]?.ApprovalLevelID !== 1 &&
                  //listApproval[approveIndex]?.ApprovalLevelID !== 2 &&
                  listApproval[approveIndex]?.ApprovalLevelID !== 7
                )
              }
              getOptionLabel={(option: ProjectModel) => option.ProjectNo || null}
              value={(projectIndex >= 0 ? listProject[projectIndex] : null)}
              onChange={(e, data: any) => {
                setProjectIndex(listProject.indexOf(data));
                setRoleData({ ...roleData, ProjectID: data ? parseInt(String(data.ProjectID)) : 0 })
              }}
              renderInput={(params) => <TextField {...params} label="Project No" disabled={approveIndex === -1} variant="outlined" margin="dense"
                error={submitted && (isNullOrUndefined(roleData.ProjectID) || projectIndex === -1)} required />}
              renderOption={(option) => <Typography style={(option.ProjectNo === "All" ? { fontWeight: 'bold' } : {})} noWrap>{option.ProjectNo}</Typography>}
            />

          </form>
        </Grid>
      </Grid>
    </Dialog>

  );
};
