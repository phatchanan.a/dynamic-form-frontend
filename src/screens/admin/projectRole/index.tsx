import React, { useState, useEffect } from "react";
import {
  Grid,
  Button,
  IconButton,
  Paper,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from '@material-ui/icons/Delete';
import { httpService } from '../../../services';

import DataTable, { Column } from '../../../components/DataTable'
import { ConfirmDlg, ResultDlg } from "../../../components/MsgDlg";
import { BaseViewModel, PaginationViewModel, SearchViewModel } from "../../../model/form";
import { Search, initSearchParams } from "../../../components/Search";
import { AppLayout } from "../../../components/AppLayout";
import EditIcon from '@material-ui/icons/Edit';
import { RoleDialog } from "./dialogRole";

export interface ApprovalModel extends BaseViewModel {
  ApprovalLevelID: number,
  ApprovalLevel: string,
  Description: string,
  ActiveFlag: string,
}

export interface UserModel {
  UserID?: number,
  UserName?: string,
  ProfileID?: number
}

export interface ProjectRoleModel extends BaseViewModel {
  ProjectRoleID?: number,
  UserID?: number,
  ProjectID?: number,
  ApprovalLevelID?: number,
  ProjectNo?: string,
  ProjectName?: string,
  UserName?: string,
  ApprovalLevel?: string,
}

export interface ProjectModel extends BaseViewModel {
  ProjectID: number,
  ProjectNo: string,
  ProjectName: string,
}

export const Role = () => {
  const [openDlg, setOpenDlg] = useState(false);
  const [projectRoleList, setProjectRoleList] = useState<ProjectRoleModel[]>([]);
  const [projectPaging, setProjectPaging] = useState<PaginationViewModel | undefined>();
  const [searchParams, setSearchParams] = useState<SearchViewModel>(initSearchParams({ order: { ProjectNo: 'desc' } }));
  const [listUser, setListUser] = useState<UserModel[]>([]);
  const [listProject, setListProject] = useState<ProjectModel[]>([]);
  const [listApproval, setListApproval] = useState<ApprovalModel[]>([]);
  const [userData, setUserData] = useState<ProjectRoleModel>();
  const [eventClickEdit, setEventClickEdit] = useState(false);

  useEffect(() => {
    getRoleDatalist(searchParams);
    httpService.get(`projectRole/FindUser`).then(response => {
      setListUser(response.items);
    })
    httpService.get(`projectRole/FindApprovalLevel`).then(response => {
      // setListApproval(response.items);
    })
    httpService.get(`projectRole/findProjectAll`).then(response => {
      // setListProject(response.items);
    })
  }, []);

  const getRoleDatalist = async (searchParams: {}) => {
    let { items, meta } = await httpService.get<ProjectRoleModel>(`projectRole/FindProjectRole`, { params: searchParams });
    setProjectRoleList(items || []);
    setProjectPaging(meta);
  }

  async function handleDeleteProjectRoleByID(deleteProjectID) {
    await httpService.delete(`projectRole/DeleteProjectRole/${deleteProjectID}`).then(response => {
      ResultDlg(response);
      getRoleDatalist(searchParams);
    });
  }

  const projectRoleListColumns: Column<ProjectRoleModel>[] = [
    {
      field: 'UserName', label: 'User Name',
    },
    { field: 'ApprovalLevel', label: 'Role' },
    { field: 'ProjectNo', label: 'Project No' },
    {
      field: 'Action', label: 'Action', headerProps: { align: 'center' }, searching: false, ordering: false,
      render: (row) => {
        return (
          <>
            <IconButton
              onClick={(e) => {
                e.stopPropagation();
                setEventClickEdit(true)
                setUserData(row);
                setOpenDlg(true)
              }}>
              <EditIcon />
            </IconButton>

            <IconButton
              onClick={(e) => {
                e.stopPropagation();
                ConfirmDlg({
                  message: 'You want to delete this item ?',
                  confirmButtonText: 'Yes, delete it!',
                  onConfirm: () => {
                    handleDeleteProjectRoleByID(row.ProjectRoleID);
                  },
                });
              }}>
              <DeleteIcon />
            </IconButton>
          </>
        );
      }
    },
  ];

  return (
    <AppLayout>
      <>
        <Grid item xs={3}>
          <Button
            // className={classes.button}
            variant="outlined"
            size="large"
            color="primary"
            startIcon={<AddIcon />}
            onClick={() => {
              setEventClickEdit(false);
              setOpenDlg(true);
            }}
          >
            Create
          </Button>
        </Grid>
        <Grid item xs={9}>

        </Grid>
      </>

      <Paper style={{ display: 'block' }}>
        <Search column={projectRoleListColumns} conditions={JSON.parse(searchParams.conditions || '[]')}
          onSearch={(data) => {
            let searchData = { ...searchParams, conditions: JSON.stringify(data), page: 1 };
            setSearchParams(searchData);
            getRoleDatalist(searchData);
          }}
        />
        <DataTable data={projectRoleList} columns={projectRoleListColumns}
          searchParams={searchParams} paging={projectPaging}
          onRender={(searchParams) => {
            setSearchParams(searchParams);
            getRoleDatalist(searchParams);
          }
          }
        />
      </Paper>

      {openDlg && <RoleDialog
        openDlg={openDlg}
        setOpenDlg={setOpenDlg}
        getRoleDatalist={getRoleDatalist}
        searchParams={searchParams}
        listUser={listUser}
        listApproval={listApproval}
        listProject={listProject}
        userData={eventClickEdit ? userData : null} />}
    </AppLayout >
  );
};
