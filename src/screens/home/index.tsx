import React, { useState, useEffect } from "react";
import {
  Grid,
  Button,
  Link,
  IconButton,
  Paper,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from '@material-ui/icons/Delete';
import { useHistory } from 'react-router-dom';
import { httpService } from '../../services';

import DataTable, { Column } from '../../components/DataTable'
import { ConfirmDlg, ResultDlg } from "../../components/MsgDlg";
import { PaginationViewModel, SearchViewModel } from "../../model/form";
import { Search, initSearchParams } from "../../components/Search";
import { AppLayout } from "../../components/AppLayout";
import { ProcessDefinitionModel } from "../../model/process-definition";

export const Home = () => {
  const history = useHistory();
  const [openDlg, setOpenDlg] = useState(false);
  const [definitionDataList, setDefinitionDataList] = useState<ProcessDefinitionModel[]>([]);
  const [projectPaging, setProjectPaging] = useState<PaginationViewModel | undefined>();
  const [searchParams, setSearchParams] = useState<SearchViewModel>(initSearchParams({ order: { ProjectNo: 'desc' } }));

  useEffect(() => {
    getDefinition(searchParams);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getDefinition = async (searchParams: {}) => {
    let { data } = await httpService.get<ProcessDefinitionModel[]>(`engine-rest/process-definition`, {});
    setDefinitionDataList(data || []);
    // setProjectPaging(meta);
  }


  const definitionDataListColumns: Column<ProcessDefinitionModel>[] = [
    {
      field: 'id', label: 'ID',
      render: (row) => {
        return (
          <Link component="button" variant="body2" style={{ fontWeight: 'bolder' }}
            onClick={() => {
              history.push("/form/" + row.key);
            }}
          >
            {row.id}
          </Link>
        );
      }
    },
    { field: 'key', label: 'Key' },
    { field: 'description', label: 'Description' },
    { field: 'name', label: 'Name' },
    { field: 'version', label: 'Version' },
    { field: 'resource', label: 'Resource' },
    { field: 'deploymentId', label: 'Deployment ID' },
    { field: 'versionTag', label: 'Version Tag' },
  ];

  return (
    <AppLayout>
      <>
        <Grid item xs={3}>
          <Button
            // className={classes.button}
            variant="outlined"
            size="large"
            color="primary"
            startIcon={<AddIcon />}
            onClick={() => {
              setOpenDlg(true);
            }}
          >
            Create Definition
          </Button>
        </Grid>
        <Grid item xs={9}>

        </Grid>
      </>

      <Paper style={{ display: 'block' }}>
        <Search column={definitionDataListColumns} conditions={JSON.parse(searchParams.conditions || '[]')}
          onSearch={(data) => {
            let searchData = { ...searchParams, conditions: JSON.stringify(data), page: 1 };
            setSearchParams(searchData);
            getDefinition(searchData);
          }}
        />
        <DataTable data={definitionDataList} columns={definitionDataListColumns}
          searchParams={searchParams} paging={projectPaging}
          onRender={(searchParams) => {
            setSearchParams(searchParams);
            getDefinition(searchParams);
          }
          }
        />
      </Paper>

      {/* {openDlg && <ProjectCreateDialog openDlg={openDlg} setOpenDlg={setOpenDlg} listPlant={listPlant} />} */}

    </AppLayout >
  );
};
