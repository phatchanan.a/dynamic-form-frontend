import React from "react";
import { makeStyles, createStyles, Theme, Typography, IconButton, FormLabel, FormControl, } from "@material-ui/core"
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Tooltip from '@material-ui/core/Tooltip';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // marginTop: theme.spacing(3),
      margin: theme.spacing(1.5),
    },
    fieldLabel: {
      marginBottom: theme.spacing(0.5),
      fontSize: 16,
    },
    required: {
      color: "#ff0000",
    },
    inputLabel: {
      fontSize: 14,
    },
    icon: {
      padding: '0px 10px',
      fontSize: 10,
    },
    radioInput: {
      marginLeft: 5,
      padding: 3,
    },
  })
);

export const SwitchPreview = (param: { [key: string]: { [key: string]: string } }) => {
  const props = Object.keys(param.properties)
    .reduce((key: any, val: any) => {
      key[val] = (param.properties[val] as any).value;
      return key;
    }, {});

  const classes = useStyles();
  const [tooltipIsOpen, setTooltipIsOpen] = React.useState(false);
  // var max = props?.max_length ? +props.max_length : 9999;
  // var validate = props?.min_length ? props?.default_value.length >= +props.min_length ? false : true : false;
  // var validate = props?.min_length ? props?.default_value ? props.default_value.length >= +props.min_length ? false : true : false : false;
  // const [validateState, setValidateState] = React.useState(validate);

  const [state, setState] = React.useState({
    checked: false
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };



  return (
    <div className={classes.root}>
      <Typography className={classes.fieldLabel}>
        <span className={classes.required}>
          {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
            'Y' === props?.required_field ? "* " : ""
          ) : ("") : ("")
          }
        </span>
        {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
          'Y' !== props?.hide_field_label ? props?.field_label : ""
        ) : ("") : ("")
        }

        {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
          props?.hint_text ? props?.hint_text.trim() === "" ? ("") : (
            <Tooltip
              open={tooltipIsOpen}
              onOpen={() => setTooltipIsOpen(true)}
              onClose={() => setTooltipIsOpen(false)}
              title={props?.hint_text} placement="right">
              <IconButton className={classes.icon} onClick={() => setTooltipIsOpen(!tooltipIsOpen)}>
                <HelpOutlineIcon />
              </IconButton>
            </Tooltip>
          ) : ("")
        ) : ("") : ("")
        }

      </Typography>

      {
        'Y' !== props?.hidden_from_content ? (
          <>
            <FormControl component="fieldset">
              <FormLabel component="legend"></FormLabel>
              <Typography component="div">
                <Grid component="label" container alignItems="center" spacing={1}>
                  {
                    props?.list
                      .sort((x: any, y: any) => x.SequenceNo - y.SequenceNo)
                      .map((item: any, index: number) => {
                        if (index === 0) {
                          return (
                            <>
                              <Grid item>{item.ListValue}</Grid>
                              <Grid item>
                                <Switch
                                  checked={state.checked}
                                  onChange={handleChange}
                                  name="checked"
                                  color="primary"
                                />
                              </Grid>
                            </>
                          )
                        } else {
                          return (<Grid item>{item.ListValue}</Grid>)
                        }
                      })
                  }
                  {/* <Grid item>
                <Switch
                  checked={state.checked}
                  onChange={handleChange}
                  name="checked"
                  color="primary"
                />
                </Grid>
                <Grid item>{t2}</Grid> */}
                </Grid>
              </Typography>
            </FormControl>
          </>
        ) : ("")
      }

    </div>
  );
}