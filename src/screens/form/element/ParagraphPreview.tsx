import React from "react";
import { TextField, makeStyles, createStyles, Theme, Typography, IconButton } from "@material-ui/core"
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // marginTop: theme.spacing(3),
      margin: theme.spacing(1.5),
    },
    textField: {
      display: "flex",
      fontSize: 14,
      '& textarea': {
        color: "#555555",
      },
    },
    disabled: {
      display: "flex",
      fontSize: 14,
      '& input': {
        color: "#555555",
      },
      background: "#DDDDDD"
    },
    fieldLabel: {
      marginBottom: theme.spacing(0.5),
      fontSize: 16,
    },
    required: {
      color: "#ff0000",
    },
    inputLabel: {
      fontSize: 14,
    },
    icon: {
      padding: '0px 10px',
      fontSize: 10,
    }
  })
);

export const ParagraphPreview = (param: { [key: string]: { [key: string]: string } }) => {
  const props = Object.keys(param.properties)
    .reduce((key: any, val: any) => {
      key[val] = (param.properties[val] as any).value;
      return key;
    }, {});

  const classes = useStyles();
  const [tooltipIsOpen, setTooltipIsOpen] = React.useState(false);
  var max = props?.max_length ? +props.max_length : 9999;
  var validate = props?.min_length ? props?.default_value ? props.default_value.length >= +props.min_length ? false : true : false : false;
  const [validateState, setValidateState] = React.useState(validate);
  
  return (
    <div className={classes.root}>
      <Typography
        className={classes.fieldLabel}
      >
        <span className={classes.required}>
        {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
          'Y' === props?.required_field ? "* " : ""
        ) : ("") : ("")
        }
        </span>

        {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
          'Y' !== props?.hide_field_label ? props?.field_label : ""
        ) : ("") : ("")
        }


        {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
          props?.hint_text ? props?.hint_text.trim() === "" ? ("") : (
            <Tooltip
              open={tooltipIsOpen}
              onOpen={() => setTooltipIsOpen(true)}
              onClose={() => setTooltipIsOpen(false)}
              title={props?.hint_text} placement="right">
              <IconButton className={classes.icon} onClick={() => setTooltipIsOpen(!tooltipIsOpen)}>
                <HelpOutlineIcon />
              </IconButton>
            </Tooltip>
          ) : ("")
        ) : ("") : ("")
        }

      </Typography>

      {
        'Y' !== props?.hidden_from_content ? (
          <TextField
            id="outlined-margin-dense"
            variant="outlined"
            margin="dense"
            defaultValue={props?.default_value}
            disabled={'Y' === props?.disabled_field}
            className={'Y' === props?.disabled_field ? classes.disabled : classes.textField}
            InputLabelProps={{ className: classes.inputLabel }}
            multiline
            rows={props?.hight}
            inputProps={{ maxLength: max }}
            onChange={(e) => {
              if (props.min_length && e.target.value.length >= +props.min_length) {
                setValidateState(false);
              }
              else {
                setValidateState(true);
              }
            }}
            error={validateState}
          />

        ) : ("")
      }


    </div>
  );
}