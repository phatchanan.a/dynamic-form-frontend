import React from "react";
import { makeStyles, createStyles, Theme, Typography } from "@material-ui/core"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // marginTop: theme.spacing(3),
      margin: theme.spacing(1.5),
    },
    fieldLabel: {
      marginBottom: theme.spacing(0.5),
      fontSize: 16,
    },
    fieldValue: {
      margin: theme.spacing(1.5),
    },
    required: {
      color: "#ff0000",
    },
    inputLabel: {
      fontSize: 14,
    },
    icon: {
      padding: '0px 10px',
      fontSize: 10,
    },
  })
);

export const LabelPreview = (param: { [key: string]: { [key: string]: string } }) => {
  const props = Object.keys(param.properties)
    .reduce((key: any, val: any) => {
      key[val] = (param.properties[val] as any).value;
      return key;
    }, {});

  const classes = useStyles();
  // var max = props?.max_length ? +props.max_length : 9999;
  // var validate = props?.min_length ? props?.default_value.length >= +props.min_length ? false : true : false;
  // const [validateState, setValidateState] = React.useState(validate);

  return (
    <div className={classes.root}>
      <Typography className={`${classes.fieldLabel}`} >
        {'Y' !== props?.hide_field_label ? props?.field_label : ""}
        <Typography className={`${classes.fieldValue}`}>
          {props?.default_value}
          {/* <InputBase
            defaultValue={props?.default_value}
            inputProps={{ 'aria-label': 'naked', readOnly: true, }}
            multiline
          /> */}
        </Typography>
      </Typography>
    </div>
  );
}