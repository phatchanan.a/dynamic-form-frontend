import React from "react";
import { TextField, makeStyles, createStyles, Theme, Typography, IconButton, FormLabel, FormControl, } from "@material-ui/core"
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Tooltip from '@material-ui/core/Tooltip';
import Autocomplete from "@material-ui/lab/Autocomplete";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // marginTop: theme.spacing(3),
      margin: theme.spacing(1.5),
    },
    fieldLabel: {
      marginBottom: theme.spacing(0.5),
      fontSize: 16,
    },
    required: {
      color: "#ff0000",
    },
    inputLabel: {
      fontSize: 14,
    },
    icon: {
      padding: '0px 10px',
      fontSize: 10,
    },
  })
);

export const DropDownPreview = (param: { [key: string]: { [key: string]: string } }) => {
  const props = Object.keys(param.properties)
    .reduce((key: any, val: any) => {
      key[val] = (param.properties[val] as any).value;
      return key;
    }, {});

  const classes = useStyles();
  const [tooltipIsOpen, setTooltipIsOpen] = React.useState(false);
  // var max = props?.max_length ? +props.max_length : 9999;
  // var validate = props?.min_length ? props?.default_value.length >= +props.min_length ? false : true : false;
  // var validate = props?.min_length ? props?.default_value ? props.default_value.length >= +props.min_length ? false : true : false : false;
  // const [validateState, setValidateState] = React.useState(validate);

  return (
    <div className={classes.root}>
      <Typography className={classes.fieldLabel}>
        <span className={classes.required}>
          {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
            'Y' === props?.required_field ? "* " : ""
          ) : ("") : ("")
          }
        </span>
        {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
          'Y' !== props?.hide_field_label ? props?.field_label : ""
        ) : ("") : ("")
        }

        {'Y' !== props?.hidden_from_content ? 'Y' !== props?.hide_field_label ? (
          props?.hint_text ? props?.hint_text.trim() === "" ? ("") : (
            <Tooltip
              open={tooltipIsOpen}
              onOpen={() => setTooltipIsOpen(true)}
              onClose={() => setTooltipIsOpen(false)}
              title={props?.hint_text} placement="right">
              <IconButton className={classes.icon} onClick={() => setTooltipIsOpen(!tooltipIsOpen)}>
                <HelpOutlineIcon />
              </IconButton>
            </Tooltip>
          ) : ("")
        ) : ("") : ("")
        }

      </Typography>

      {
        'Y' !== props?.hidden_from_content ? (<>
          <FormControl component="fieldset">
            <FormLabel component="legend"></FormLabel>

            <Autocomplete
              id="combo-box-demo"
              options={props?.list.sort((x: any, y: any) => x.SequenceNo - y.SequenceNo)}
              getOptionLabel={(option: any) => option.ListValue}
              // value={(option: any) => option.ListKey}
              style={{ width: 300 }}
              renderInput={(params) => <TextField {...params} label="Please select" variant="outlined" margin="dense" />}
              renderOption={(option) => <Typography noWrap>{option.ListValue}</Typography>}
            />

          </FormControl>

        </>) : ("")
      }

    </div>
  );
}