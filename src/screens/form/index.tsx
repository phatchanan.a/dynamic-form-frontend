import React, { useEffect, useState } from "react";
import { Button, Grid, Paper, Typography } from "@material-ui/core";
// import { PropsForm } from "../../../reducers/reducers.interface";
import { useSelector } from "react-redux";
import { FieldTextbox } from "./fields/FieldTextbox";
// import { FormDetails } from "../../../model/form";
import { ParagraphPreview } from "./element/ParagraphPreview";
import { LabelPreview } from "./element/LabelPreview";
import { TitleDescriptionPreview } from "./element/TitleDescriptionPreview";
import { MultipleChoicePreview } from "./element/MultipleChoicePreview";
import { CheckBoxPreview } from "./element/CheckBoxPreview";
import { DropDownPreview } from "./element/DropDownPreview";
import { DateTimePreview } from "./element/DateTimePreview";
import { YesNoPreview } from "./element/YesNoPreview";
import { SwitchPreview } from "./element/SwitchPreview";
import { ImagePreview } from "./element/ImagePreview";
import { PunchListPreview } from "./element/PunchListPreview";
import { PunchRemarkPreview } from "./element/PunchRemarkPreview";

// import Dialog from "../../../components/Dialog";
import { theme } from "../../Theme/Theme";
import { AppLayout } from "../../components/AppLayout";
import { useHistory, useParams } from "react-router-dom";
import { httpService } from "../../services";
import { FormFieldsViewModel, FormViewModel } from "../../model/form";
import LabelImportantIcon from '@material-ui/icons/LabelImportant';

export const Form = (props: any) => {

  let params: any = useParams();
  const formKey = params.formKey;

  const history = useHistory();
  const [formData, setFormData] = useState<FormViewModel>({});

  useEffect(() => {
    getFormData();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getFormData = async () => {
    let { data } = await httpService.get<FormViewModel>(`rest/process-definition/key/${formKey}/form-variables`, {});
    setFormData(data || {});
  }

  return (
    <AppLayout>
      <>
        <Grid item xs={3}>
          {formKey}
        </Grid>
        <Grid item xs={9}>

        </Grid>
      </>

      <Paper style={{ display: 'block' }}>
        <Grid item xs={6}>
          {
            (formData.formFields || []).map((field: FormFieldsViewModel, idx: number) => {
              return (
                // <div key={idx} style={{ border: '2px #bbb dashed', margin: theme.spacing(2), borderRadius: theme.spacing(1) }}></div>
                <BuildPreview {...field} />
              )
            })
          }

          {(formData.formFields || []).length !== 0 && <Button
            variant="contained"
            color="primary"
            startIcon={<LabelImportantIcon />}
            style={{ margin: theme.spacing(1) }}
            // className={`${classes.btn_approve}`}
            // disabled={selected}
            onClick={() => {
              // updateInspApproval("Approve", remark, [{ InspID: list.InspectID }]);
            }}
          >
            Start Workflow
          </Button>}
        </Grid>
      </Paper>

    </AppLayout >
  );
}


const BuildPreview = (field: FormFieldsViewModel) => {
  // const { field } = param;
  // console.log(field);
  if ('string' === field.type.name) {
    return (
      <FieldTextbox {...field} />
    )
  }

  // else if ('paragraph' === field.fieldKey) {
  //   return (
  //     <ParagraphPreview properties={field.properties} />
  //   )
  // }

  // else if ('label' === field.fieldKey) {
  //   return (
  //     <LabelPreview properties={field.properties} />
  //   )
  // }

  // else if ('title_and_description' === field.fieldKey) {
  //   return (
  //     <TitleDescriptionPreview properties={field.properties} />
  //   )
  // }

  // else if ('radio_button' === field.fieldKey) {
  //   return (
  //     <MultipleChoicePreview properties={field.properties} />
  //   )
  // }

  // else if ('check_box' === field.fieldKey) {
  //   return (
  //     <CheckBoxPreview properties={field.properties} />
  //   )
  // }

  // else if ('dropdown' === field.fieldKey) {
  //   return (
  //     <DropDownPreview properties={field.properties} />
  //   )
  // }

  // else if ('date_time' === field.fieldKey) {
  //   return (
  //     <DateTimePreview properties={field.properties} />
  //   )
  // }

  // else if ('yes_no' === field.fieldKey) {
  //   return (
  //     <YesNoPreview properties={field.properties} />
  //   )
  // }

  // else if ('switch' === field.fieldKey) {
  //   return (
  //     <SwitchPreview properties={field.properties} />
  //   )
  // }

  // else if ('image' === field.fieldKey) {
  //   return (
  //     <ImagePreview properties={field.properties} />
  //   )
  // }

  // else if ('punch_list' === field.fieldKey) {
  //   return (
  //     <PunchListPreview properties={field.properties} />
  //   )
  // }

  // else if ('punch_remark' === field.fieldKey) {
  //   return (
  //     <PunchRemarkPreview properties={field.properties} />
  //   )
  // }

  return <></>;
}