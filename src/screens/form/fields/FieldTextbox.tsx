import React from "react";
import { TextField, makeStyles, createStyles, Theme, Typography, IconButton, } from "@material-ui/core"
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Tooltip from '@material-ui/core/Tooltip';
import { FormFieldsViewModel } from "../../../model/form";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // marginTop: theme.spacing(3),
      margin: theme.spacing(1),
    },
    textField: {
      display: "flex",
      fontSize: 14,
      '& input': {
        color: "#555555",
      },
    },
    disabled: {
      display: "flex",
      fontSize: 14,
      '& input': {
        color: "#555555",
      },
      background: "#DDDDDD"
    },
    fieldLabel: {
      marginBottom: theme.spacing(0.5),
      fontSize: 16,
    },
    required: {
      color: "#ff0000",
    },
    inputLabel: {
      fontSize: 14,
    },
    icon: {
      padding: '0px 10px',
      fontSize: 10,
    }
  })
);

export const FieldTextbox = (param: FormFieldsViewModel) => {
  // const props = Object.keys(param.properties)
  //   .reduce((key: any, val: any) => {
  //     key[val] = (param.properties[val] as any).value === "" ? null : (param.properties[val] as any).value;
  //     return key;
  //   }, {});

  const classes = useStyles();
  const [validateState, setValidateState] = React.useState(false);

  return (
    <div className={classes.root}>
      {
        <TextField
          fullWidth
          variant="outlined"
          margin="dense"
          required
          id={param?.id}
          label={param?.label}
          defaultValue={param?.defaultValue}
          // disabled={'Y' === param?.disabled_field}
          // className={'Y' === param?.disabled_field ? classes.disabled : classes.textField}
          InputLabelProps={{ className: classes.inputLabel }}
          // multiline={props?.hight ? true : false}
          // rows={props?.hight}
          onChange={(e) => {
            // if (param.min_length && e.target.value.length < +param.min_length) {
            //   setValidateState(true);
            // }
            // else {
            //   setValidateState(false);
            // }
          }}
          error={validateState}
        />
      }
    </div>
  );
}