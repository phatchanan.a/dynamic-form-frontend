import React, { useCallback, useState } from "react";
import "./login.scss";
import {
  TextField,
  Card,
  CardContent,
  CardActions,
  Button,
  makeStyles,
  Tooltip,
  TooltipProps,
} from "@material-ui/core";
import { Formik } from "formik";
import Img from "../../assets/image/shutterstock_298434788Fs.jpg";
import Logo from "../../assets/image/scg.png";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import userManager from "../../userManager";
import { httpService } from "../../services";
import { ResultDlg } from "../../components/MsgDlg";
import { loginSuccess } from "../../routes/AppRouter";

// import { useKeycloak } from '@react-keycloak/web'
// import type { KeycloakInstance } from 'keycloak-js'

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiFormHelperText-root': {
      lineHeight: 1,
    },
    '& .MuiTextField-root': {
      // width: '80%',
      // marginBottom: 0,
    }
  },
  scgUserBotton: {
    marginTop: theme.spacing(1),
    background: theme.palette.error.main,
    color: "#FAFAFA",
    "&:hover": {
      background: theme.palette.error.dark,
    },
  },
}));

const useStylesFormTooltip = makeStyles((theme) => ({
  arrow: {
    width: '0.75em !important',
    color: theme.palette.error.dark,
  },
  tooltip: {
    lineHeight: 2,
    backgroundColor: theme.palette.error.dark,
  },
}));

function FormTooltip(props: TooltipProps) {
  const { title, open, children } = props;
  const classes = useStylesFormTooltip();

  return <Tooltip open={open} arrow classes={classes} title={title} placement="right" children={children} TransitionProps={{ timeout: 0 }} />;
}

const validationSchema = Yup.object().shape({
  userName: Yup.string()
    // .test({
    //   name: 'validator-custom-name',
    //   // eslint-disable-next-line object-shorthand
    //   test: function (value) {
    //     // You can add any logic here to generate a dynamic message

    //     return value === 1
    //       ? this.createError({
    //         message: `Custom Message here ${value}`,
    //         path: 'fieldName', // Fieldname
    //       })
    //       : true;
    //   }
    // })
    .min(5, "Must have 5 character more than or equal")
    .max(50, "Must be shorter than 50")
    .required("Must enter a username"),
  password: Yup.string()
    // .email("Must be a valid email address")
    .min(5, "Must have 5 character more than or equal")
    .max(50, "Must be shorter than 50")
    .required("Must enter a password"),
});

const validatePassword = Yup.object().shape({
  password: Yup.string()
    .min(5, "Must have 5 character more than or equal")
    .max(50, "Must be shorter than 50")
    .required("Must enter a password"),
  confirmPassword: Yup.string()
    .min(5, "Must have 5 character more than or equal")
    .max(50, "Must be shorter than 50")
    .required("Must enter a confirm password")
    .oneOf([Yup.ref('password'), null], 'Passwords must match'),
  // .test('passwords-match', 'Passwords must match', function (value) {
  //   return this.parent.password === value
  // })
});

interface ILogin {
  userName?: string;
  password?: string;
}

export const Login = (props: any) => {
  const { setAccessTokens, setCurUser, curUser } = props;
  const classes = useStyles();
  const history = useHistory();
  // const { keycloak } = useKeycloak<KeycloakInstance>();
  const [oldLoginData, setOldLoginData] = useState<ILogin>(null);
  const [loginData, setLoginData] = useState<{
    LocalUserID?: number,
    change_password?: boolean,
    access_token?: boolean,
  }>(null);

  const login = useCallback(() => {

    const state_reg = new RegExp('[&]state=\\w*');
    const scope_reg = new RegExp('[&]scope=\\w*');
    const code_chellenge_reg = new RegExp('[&]code_challenge=\\w*');
    const code_chellenge_method_reg = new RegExp('[&]code_challenge_method=\\w*');

    userManager.createSigninRequest().then(res => {
      res.url = res.url.replace(state_reg, ''); //remove state_reg from url
      res.url = res.url.replace(scope_reg, ''); //remove scope from url
      res.url = res.url.replace(code_chellenge_reg, ''); //remove code_chellenge from url
      res.url = res.url.replace(code_chellenge_method_reg, ''); //remove code_chellenge_method from url
      window.location.href = res.url;
    }).catch(err => {
      console.log(err);
    });
  }, [userManager])

  // if (keycloak?.authenticated) return <Redirect to={{ pathname: '/' }} />

  const submitLogin = (values: ILogin, { setSubmitting, resetForm }: any) => {
    setSubmitting(true);

    httpService.post(`localuser/Validate`, { UserName: values.userName, Password: values.password }).then(res => {
      // debugger

      // if (!res.result) ResultDlg(res, { onClose: () => { setSubmitting(false) }, message: 'The username or password is incorrect. Please try again.' });
      if (!res.result) ResultDlg(res, { onClose: () => { setSubmitting(false) }, title: '' });
      else
        if (res.item.access_token) {
          loginSuccess(res, setAccessTokens, setCurUser, 'local');
          // history.push(localStorage.getItem("redirectURL") || '/');
        } else if (res.item.LocalUserID && res.item.change_password) {
          resetForm();
          setOldLoginData(values);
          setLoginData(res.item);
        }

    });

  };

  const submitChangePassword = (values, { setSubmitting, resetForm }: any) => {
    setSubmitting(true);

    httpService.put(`localuser/${loginData.LocalUserID}`, { UserName: oldLoginData.userName, Password: values.password }).then(res => {

      if (!res.result) ResultDlg(res, { onClose: () => { setSubmitting(false) } });
      else
        if (res.item.access_token) {
          loginSuccess(res, setAccessTokens, setCurUser, 'local');
          // history.push(localStorage.getItem("redirectURL") || '/');
        }

    });

  };

  return (
    <div className={`${classes.root} layout-login`}>
      <div className="bg-login"></div>
      <Card className="card-login" elevation={5}>
        <div className="img-logo">
          <img className="image" src={Img} alt="img-scg" />
        </div>

        {!loginData?.change_password && <CardContent className="card-content">
          <div className="card-header">
            <img className="login-logo" src={Logo} alt="logo-scg" />
            {/* <span className="title-text">Login</span> */}
            <span className="sub-title">Welcome to SCG Chemicals Co.,Ltd.</span>
            <span className="sub-title">Sign in by entering the information below</span>
            <br />
            <span className="title-text">Login via SCG User</span>
            <Button
              className={`${classes.scgUserBotton}`}
              // color="secondary"
              variant="contained"
              onClick={login}
            >
              SCG User
            </Button>
            <br />
            <br />
            <span className="title-text">Login via Non-SCG User</span>
          </div>

          <Formik
            initialValues={{ userName: "", password: "" }}
            validationSchema={validationSchema}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              submitLogin(values, { setSubmitting, resetForm })
            }
            }
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
              <form className="login-form" onSubmit={handleSubmit} id="loginForm" noValidate>
                <FormTooltip open={touched.userName && errors.userName ? true : false} title={errors.userName}>
                  <TextField
                    autoComplete="off"
                    required={true}
                    label="Username"
                    variant="outlined"
                    margin="dense"
                    type="text"
                    // name="userName"
                    id="userName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.userName}
                    error={touched.userName && errors.userName ? true : false}
                  // helperText={
                  //   touched.userName && errors.userName
                  //     ? errors.userName
                  //     : ""
                  // }
                  />
                </FormTooltip>

                <FormTooltip open={touched.password && errors.password ? true : false} title={errors.password}>
                  <TextField
                    required={true}
                    label="Password"
                    variant="outlined"
                    margin="dense"
                    type="password"
                    // name="password"
                    id="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                    error={touched.password && errors.password ? true : false}
                  // helperText={
                  //   touched.password && errors.password
                  //     ? errors.password
                  //     : " "
                  // }
                  />
                </FormTooltip>

                <CardActions className="card-actions">
                  <input type="submit" form={"loginForm"} style={{ display: 'none' }} />
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => { handleSubmit() }}
                    disabled={isSubmitting}
                  >
                    Login
                    </Button>
                </CardActions>
              </form>
            )}
          </Formik>
        </CardContent>}

        {loginData?.change_password && <CardContent className="card-content">
          <div className="card-header">
            <img className="login-logo" src={Logo} alt="logo-scg" />
            {/* <span className="title-text">Login</span> */}
            <span className="sub-title">Welcome to SCG Chemicals Co.,Ltd.</span>
            <span className="sub-title">Sign in by entering the information below</span>
            <br />
            <br />
            <span className="title-text">Change password for first login</span>
          </div>

          <Formik
            initialValues={{ password: "", confirmPassword: "" }}
            validationSchema={validatePassword}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              submitChangePassword(values, { setSubmitting, resetForm })
            }
            }
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
              <form className="login-form" onSubmit={handleSubmit} id="changePasswordForm" noValidate>
                <FormTooltip open={touched.password && errors.password ? true : false} title={errors.password}>
                  <TextField
                    required={true}
                    label="Password"
                    variant="outlined"
                    margin="dense"
                    type="password"
                    // name="password"
                    id="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                    error={touched.password && errors.password ? true : false}
                  // helperText={
                  //   touched.password && errors.password
                  //     ? errors.password
                  //     : " "
                  // }
                  />
                </FormTooltip>

                <FormTooltip open={touched.confirmPassword && errors.confirmPassword ? true : false} title={errors.confirmPassword}>
                  <TextField
                    required={true}
                    label="Confirm Password"
                    variant="outlined"
                    margin="dense"
                    type="password"
                    // name="confirmPassword"
                    id="confirmPassword"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.confirmPassword}
                    error={touched.confirmPassword && errors.confirmPassword ? true : false}
                  // helperText={
                  //   touched.confirmPassword && errors.confirmPassword
                  //     ? errors.confirmPassword
                  //     : " "
                  // }
                  />
                </FormTooltip>

                <CardActions className="card-actions">
                  <input type="submit" form={"changePasswordForm"} style={{ display: 'none' }} />
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => { handleSubmit() }}
                    disabled={isSubmitting}
                  >
                    Change Password
                    </Button>
                </CardActions>
              </form>
            )}
          </Formik>
        </CardContent>}

      </Card>
    </div>
  );
};
