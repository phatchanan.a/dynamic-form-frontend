import React from "react";
import "./login.scss";
import {
  TextField,
  Card,
  CardContent,
  CardActions,
  Button,
  makeStyles,
  Tooltip,
  TooltipProps,
} from "@material-ui/core";
import { Formik } from "formik";
import Img from "../../assets/image/shutterstock_298434788Fs.jpg";
import Logo from "../../assets/image/scg.png";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import { httpService } from "../../services";
import { ResultDlg } from "../../components/MsgDlg";
import { loginSuccess } from "../../routes/AppRouter";

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiFormHelperText-root': {
      lineHeight: 1,
    },
    '& .MuiTextField-root': {
      // width: '80%',
      // marginBottom: 0,
    }
  },
  scgUserBotton: {
    marginTop: theme.spacing(1),
    background: theme.palette.error.main,
    color: "#FAFAFA",
    "&:hover": {
      background: theme.palette.error.dark,
    },
  },
}));

const useStylesFormTooltip = makeStyles((theme) => ({
  arrow: {
    width: '0.75em !important',
    color: theme.palette.error.dark,
  },
  tooltip: {
    lineHeight: 2,
    backgroundColor: theme.palette.error.dark,
  },
}));

function FormTooltip(props: TooltipProps) {
  const { title, open, children } = props;
  const classes = useStylesFormTooltip();

  return <Tooltip open={open} arrow classes={classes} title={title} placement="right" children={children} TransitionProps={{ timeout: 0 }} />;
}

const validatePassword = Yup.object().shape({
  password: Yup.string()
    .min(5, "Must have 5 character more than or equal")
    .max(50, "Must be shorter than 50")
    .required("Must enter a password"),
  confirmPassword: Yup.string()
    .min(5, "Must have 5 character more than or equal")
    .max(50, "Must be shorter than 50")
    .required("Must enter a confirm password")
    .oneOf([Yup.ref('password'), null], 'Passwords must match'),
  // .test('passwords-match', 'Passwords must match', function (value) {
  //   return this.parent.password === value
  // })
});

export const ResetPassword = (props: any) => {
  const { setAccessTokens, setCurUser, curUser } = props;
  const classes = useStyles();
  const history = useHistory();
  // const { keycloak } = useKeycloak<KeycloakInstance>();

  const submitChangePassword = (values, { setSubmitting, resetForm }: any) => {
    setSubmitting(true);

    httpService.put(`localuser/${curUser.LocalUserID}`, { UserName: curUser.UserName, Password: values.password }).then(res => {

      if (!res.result) ResultDlg(res, { onClose: () => { setSubmitting(false) } });
      else
        if (res.item.access_token) {
          loginSuccess(res, setAccessTokens, setCurUser, 'local');

          setTimeout(() => {
            history.push("/");
          }, 500);
        }

    });

  };

  return (
    <div className={`${classes.root} layout-login`}>
      <div className="bg-login"></div>
      <Card className="card-login" elevation={5}>
        <div className="img-logo">
          <img className="image" src={Img} alt="img-scg" />
        </div>
        <CardContent className="card-content">
          <div className="card-header">
            <img className="login-logo" src={Logo} alt="logo-scg" />
            {/* <span className="title-text">Login</span> */}
            <span className="sub-title">Welcome to SCG Chemicals Co.,Ltd.</span>
            <span className="sub-title">Change Password by entering the information below</span>
            <br />
            <br />
            <span className="title-text">New Password</span>
          </div>

          <Formik
            initialValues={{ password: "", confirmPassword: "" }}
            validationSchema={validatePassword}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              submitChangePassword(values, { setSubmitting, resetForm })
            }
            }
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
                <form className="login-form" onSubmit={handleSubmit} id="changePasswordForm" noValidate>
                  <FormTooltip open={touched.password && errors.password ? true : false} title={errors.password}>
                    <TextField
                      required={true}
                      label="Password"
                      variant="outlined"
                      margin="dense"
                      type="password"
                      // name="password"
                      id="password"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.password}
                      error={touched.password && errors.password ? true : false}
                    // helperText={
                    //   touched.password && errors.password
                    //     ? errors.password
                    //     : " "
                    // }
                    />
                  </FormTooltip>

                  <FormTooltip open={touched.confirmPassword && errors.confirmPassword ? true : false} title={errors.confirmPassword}>
                    <TextField
                      required={true}
                      label="Confirm Password"
                      variant="outlined"
                      margin="dense"
                      type="password"
                      // name="confirmPassword"
                      id="confirmPassword"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.confirmPassword}
                      error={touched.confirmPassword && errors.confirmPassword ? true : false}
                    // helperText={
                    //   touched.confirmPassword && errors.confirmPassword
                    //     ? errors.confirmPassword
                    //     : " "
                    // }
                    />
                  </FormTooltip>

                  <CardActions className="card-actions">
                    <input type="submit" form={"changePasswordForm"} style={{ display: 'none' }} />
                    <Button
                      color="primary"
                      variant="contained"
                      onClick={() => { handleSubmit() }}
                      disabled={isSubmitting}
                    >
                      Change Password
                    </Button>
                  </CardActions>
                </form>
              )}
          </Formik>
        </CardContent>

      </Card>
    </div>
  );
};
